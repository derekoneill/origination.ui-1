pipeline {

    agent { label 'slc-qasappjla01-Linux' }

    environment {
        DOCKER_REGISTRY='docker-base.art.proginternal.net'
        DOCKER_CRED='Artifactory'
        DOCKER_IMAGE_BUILDER_NAME='dotnet3-node14-builder'
        IMAGE_TAG='1.4'
    }

    stages {
        stage('DockerBuild'){
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'Artifactory', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]) {
                        sh "docker login -u=$DOCKER_USERNAME -p=$DOCKER_PASSWORD $DOCKER_REGISTRY"
                        sh "docker build --rm -t $DOCKER_REGISTRY/$DOCKER_IMAGE_BUILDER_NAME:$IMAGE_TAG -f ci/Dockerfile.builder ."

                        // push the newly create image to our artifactory.
                        sh "docker push $DOCKER_REGISTRY/$DOCKER_IMAGE_BUILDER_NAME:$IMAGE_TAG"

                        // list all the current docker images.
                        sh "docker images"

                        // remove all unused images.
                        sh 'docker image prune -f'

                        // list all the current docker images.
                        sh "docker images"

                        sh "docker logout"
                    }
                }
            }
        }
    }
}
