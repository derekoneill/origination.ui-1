#!groovy

pipeline {
	agent { label 'slc-qasappjla01-Linux' }

	options {
		//Only one concurrent build at a time on this project
        disableConcurrentBuilds()
        //Build discard policy
        buildDiscarder(logRotator(numToKeepStr: '5', daysToKeepStr: '5'))
	}

    parameters {
        // temporarily use forge channel and decide if we want to create a new channel and replace it here.
        string(name: 'SlackChannel', defaultValue: '#ua-automatons', description: 'The name of the slack channel that you want build notifications published to.')
        string(name: 'BranchName', defaultValue: 'master', description: 'Enter the branch you want to deploy.')
    }

	environment {
        // These environments are required by this ci template and this template will be reused by other's projects and we can parametize these
        // to support other project in the future.
        REPO_NAME = "origination.ui"
		PROJECT_NAME = "origination.ui"
        SOFTWARE_NAME = "ProgApply"
        CLIENT_APP_ROOT = "client-apps"
        GIT_TAG = "${sh(returnStdout: true, script: 'git log -1 --pretty=%h').trim()}"
		ARTIFACTORY_URL = "https://art.proginternal.net"
        SONARQUBE_URL = "http://10.100.63.34:9000"
        IMAGE_BUILDER = "docker-base.art.proginternal.net/dotnet3-node14-builder:1.4"
	}

	stages {
        stage('Checkout and print environments') {
            steps {
                script {
                    if (!env.GIT_BRANCH.contains(params.BranchName)) {
                        echo "Deploying branch (${params.BranchName}) is different than default configured branch (${env.GIT_BRANCH})"
                        scmRepo = checkout([$class: 'GitSCM', branches: [[name: "*/${params.BranchName}"]], doGenerateSubmoduleConfigurations: false, submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'bitbucket', url: "https://bitbucket.org/progfin-ondemand/${env.REPO_NAME}.git"]]])
                        GIT_TAG = "${scmRepo.GIT_COMMIT}".substring(0,7)
                    }

                    // print out the environments information.
                    sh 'printenv'
                }
			}
		}

        stage('Verify Service') {
            steps {
                script {
                    withCredentials([string(credentialsId: 'Sonarqube', variable: 'token')]) {
                        // verify service:  run unit tests and start code analysis with sonarqube as well if we failed any of them, we will fail the build.
                        sh "docker run --network host -t --rm -v $WORKSPACE:/workspace $IMAGE_BUILDER bash -c \
                            'dotnet sonarscanner begin -k:${PROJECT_NAME}_service -d:sonar.login=$token -d:sonar.host.url=$SONARQUBE_URL -n:$REPO_NAME -d:sonar.verbose=true -v:$BUILD_NUMBER -d:sonar.exclusions=**/$CLIENT_APP_ROOT/**,**/bin/**,**/dist/**,**/obj/** && \
                            dotnet restore && dotnet build -c Release && dotnet test && \
                            dotnet sonarscanner end -d:sonar.login=$token'"
                    }
                }
            }
        }

        stage('Verify Presentation') {
            steps {
                script {
                    withCredentials([string(credentialsId: 'Sonarqube', variable: 'token')]) {
                        // Verify the presentation: by running unit tests and potentially we would add e2e test here as well and start code quality analysis with sonar qube.
                        sh "docker run --network host -t --rm -v $WORKSPACE/$PROJECT_NAME/$CLIENT_APP_ROOT:/workspace $IMAGE_BUILDER npm install --verbose"
                        sh "docker run --network host -t --rm -v $WORKSPACE/$PROJECT_NAME/$CLIENT_APP_ROOT:/workspace $IMAGE_BUILDER npm run test"
                        def sonarScannerCliImg = "sonarsource/sonar-scanner-cli"
                        sh "docker run --network host -t --rm -e SONAR_LOGIN=$token -v $WORKSPACE/$PROJECT_NAME/$CLIENT_APP_ROOT:/usr/src $sonarScannerCliImg -X -Dsonar.verbose=true -Dproject.settings=./sonar-project.properties -Dsonar.host.url=$SONARQUBE_URL "
                    }
                }
            }
        }

        stage('Build and bundle assets') {
            // this is the step after we verify all our tests and static code analysis to build and bundle all the assets together here.
            steps {
                script {
                    sh "docker run --network host -t --rm -v $WORKSPACE/$PROJECT_NAME/:/workspace $IMAGE_BUILDER dotnet restore"
                    sh "sudo rm -rf $WORKSPACE/$PROJECT_NAME/publish"
                    sh "docker run --network host -t --rm -v $WORKSPACE/$PROJECT_NAME/:/workspace $IMAGE_BUILDER dotnet publish -c Release -r win-x64 --self-contained true /p:AppendBaseHref=true /p:useapphost=true --no-dependencies -v:q -o /workspace/publish"
                }
            }
        }


        stage('Push to Artifactory') {
            steps {
                // this is the step when everything is ready to push up to the artifactory and kick off all the default deployment environments.
                // for now we will try to continue delivering our code to kubernetes and lagacy system, such as IIS farms(need to go through CM team's CD pipelines.)
                script {
                    sh "rm -rf *.zip *.tar.gz"

                    def now = new Date()
                    def artifactDate = now.format("yyyyMMdd", TimeZone.getTimeZone('UTC'))
                    def artifactFileName = "${SOFTWARE_NAME}-${artifactDate}-${env.GIT_TAG}-${env.BUILD_NUMBER}.zip"

                    pwsh(returnStdout: true, label: 'Zip Artifacts', script: "Compress-Archive $WORKSPACE/$PROJECT_NAME/publish/* $artifactFileName")
                    sh "ls -la $WORKSPACE"

                    withCredentials([string(credentialsId: 'ArtifactoryAPIKey', variable: 'API_KEY')]) {
                        //Push artifact to artifactory
                        sh "jfrog rt u $artifactFileName 'virtual-progleasing/$SOFTWARE_NAME/$artifactFileName' --build-name=$SOFTWARE_NAME --build-number=$env.BUILD_NUMBER --url='$ARTIFACTORY_URL/artifactory' --apikey=$API_KEY"
                        //Publish Build
                        sh "jfrog rt bp $SOFTWARE_NAME $env.BUILD_NUMBER --url='$ARTIFACTORY_URL/artifactory' --apikey=$API_KEY"
                    }
                }
            }
        }
    }

    post {
        unstable {
            script {
                slackSend channel: params.SlackChannel, color: 'warning', message: "Build was unstable.  A quality gate or test has failed.  ${env.BUILD_URL}", tokenCredentialId: "${params.SlackChannel}"
            }
        }
        fixed {
            script {
                slackSend channel: params.SlackChannel, color: 'good', message: "Build has recovered. ${env.BUILD_URL}", tokenCredentialId: "${params.SlackChannel}"
            }
        }
        failure {
            script {
                slackSend channel: params.SlackChannel, color: 'danger', message: "Build has failed. ${env.BUILD_URL}", tokenCredentialId: "${params.SlackChannel}"
            }
        }
    }
}
