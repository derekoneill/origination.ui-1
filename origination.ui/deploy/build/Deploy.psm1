Function CopyBundle {
  Param(
      [parameter(mandatory = $true, position = 0)][string]$instanceId
  )

  $awsFolder = Join-Path -Path $PSScriptRoot -ChildPath "../aws"
  $ec2Path = Join-Path -Path $awsFolder -ChildPath "EC2.psm1"
  Import-Module $ec2Path -Force
  $keyPath = Join-Path -Path $awsFolder -ChildPath "ProgApply.pem"

  $sourceFolder = Join-Path -Path $PSScriptRoot -ChildPath "/../../bin/deploy"

  $instance = GetInstanceByInstanceId $instanceId
  $instanceIp = $instance.PublicIpAddress

  Write-Host "Accessing key at $keyPath"
  Write-Host "Copying artifacts to staging instance $instanceId ..."
  
  Push-Location $sourceFolder

  if ( !(Test-Path $sourceFolder) ) {
      Write-Error -Message "No artifacts in $sourceFolder" -ErrorAction Stop
      exit
  }
  scp -i $keyPath -o StrictHostKeyChecking=no -r "$(pwd)" "ec2-user@${instanceIp}:/var/www"
  Push-Location $PSScriptRoot
}

Export-ModuleMember -Function CopyBundle