$timer = new-object system.diagnostics.stopwatch
$timer.start()

try {    
  $buildFolder = $PSScriptRoot
  $lintJobResult = @{ }
  $lintJobScript = [PowerShell]::Create().AddScript( {
          param($buildFolder, $lintJobResult)
          $qualityPath = Join-Path -Path $buildFolder -ChildPath 'QualityChecks.psm1'
          Import-Module $qualityPath -Force       

          UiLint
          $lintJobResult.exitCode = $LASTEXITCODE
      }).AddArgument($buildFolder).AddArgument($lintJobResult)
  $lintJob = $lintJobScript.BeginInvoke()

  $uiTestJobResult = @{ }
  $uiTestJobScript = [PowerShell]::Create().AddScript( {
          param($buildFolder, $uiTestJobResult)
          $qualityPath = Join-Path -Path $buildFolder -ChildPath 'QualityChecks.psm1'
          Import-Module $qualityPath -Force       

          RunUiTests
          $uiTestJobResult.exitCode = $LASTEXITCODE
      }).AddArgument($buildFolder).AddArgument($uiTestJobResult)
  $uiTestJob = $uiTestJobScript.BeginInvoke()

  $dotnetTestJobResult = @{ }
  $dotnetTestJobScript = [PowerShell]::Create().AddScript( {
          param($buildFolder, $dotnetTestJobResult)
          $qualityPath = Join-Path -Path $buildFolder -ChildPath 'QualityChecks.psm1'
          Import-Module $qualityPath -Force       

          RunDotnetTests
          $dotnetTestJobResult.exitCode = $LASTEXITCODE
      }).AddArgument($buildFolder).AddArgument($dotnetTestJobResult)
  $dotnetTestJob = $dotnetTestJobScript.BeginInvoke()

  $bumbleJobResult = @{ }
  $bumbleJobScript = [PowerShell]::Create().AddScript( {
          param($buildFolder, $bumbleJobResult)
          $compilePath = Join-Path -Path $buildFolder -ChildPath 'Compile.psm1'
          Import-Module $compilePath -Force       

          BuildBundle
          $bumbleJobResult.exitCode = $LASTEXITCODE
      }).AddArgument($buildFolder).AddArgument($bumbleJobResult)
  $bumbleJob = $bumbleJobScript.BeginInvoke()
  
  Write-Host ""
  Write-Host "Linting the UI code..."
  $lintJobScript.EndInvoke($lintJob)
  if ( $lintJobResult.exitCode -ne 0) {
      throw "Linting the UI code failed"
  }
  
  Write-Host ""
  Write-Host "Testing the UI code..."
  $uiTestJobScript.EndInvoke($uiTestJob)
  if ( $uiTestJobResult.exitCode -ne 0) {
      throw "Testing the UI code failed"
  }
  
  Write-Host ""
  Write-Host "Testing the dotnet code..."
  $dotnetTestJobScript.EndInvoke($dotnetTestJob)
  if ( $dotnetTestJobResult.exitCode -ne 0) {
      throw "Testing the dotnet code failed"
  }
  
  Write-Host ""
  Write-Host "Bumbling the package..."
  $bumbleJobScript.EndInvoke($bumbleJob)
  if ( $bumbleJobResult.exitCode -ne 0) {
      throw "Bumbling the package failed"
  }
  
  $result = "succeeded"
}
catch {
  $result = "failed"
  Write-Host $_.Exception.Message
}

$timer.stop()
$elapsedTime = [math]::Round($timer.elapsedmilliseconds / 1000)

""
Write-Host "Build $result in $($elapsedTime) seconds."