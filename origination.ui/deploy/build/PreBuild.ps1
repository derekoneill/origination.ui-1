$testsPath = Join-Path -Path $PSScriptRoot -ChildPath 'QualityChecks.psm1'
Import-Module $testsPath -Force

UiLint
if ($LASTEXITCODE -ne 0) {
  throw 'Ui Linting failed'
}

RunUiTests
if ($LASTEXITCODE -ne 0) {
  throw 'Ui Test(s) failed'
}

RunDotnetTests 
if ($LASTEXITCODE -ne 0) {
  throw 'Dotnet wrapper Test(s) failed'
}
