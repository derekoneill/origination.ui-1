Function BuildBundle {
  $originalLocation = $PSScriptRoot
  $projectRootPath = Join-Path -Path $PSScriptRoot -ChildPath '../../../'
  Set-Location $projectRootPath

  dotnet publish -c Release -r linux-x64 -o origination.ui/bin/deploy

  Set-Location $originalLocation
}

Export-ModuleMember -Function BuildBundle