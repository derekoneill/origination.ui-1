Import-Module "$PSScriptRoot/../aws/EC2.psm1" -force
Import-Module "$PSScriptRoot/../aws/Production-Host.psm1" -force

$timer = new-object system.diagnostics.stopwatch
$timer.start()
$newHostId = CreateEc2Instance
Write-Host "new id $newHostId"


    # $qualifyScriptFolder = Resolve-Path -Path "$PSScriptRoot"
    # $qualifyJobResult = @{ }
    # $qualifyJobScript = [PowerShell]::Create().AddScript( {
    #         param($qualifyScriptFolder, $qualifyJobResult)        
    #         . "$qualifyScriptFolder/qualifyUiBuild.ps1"
    #         $qualifyJobResult.exitCode = $LASTEXITCODE
    #     }).AddArgument($qualifyScriptFolder).AddArgument($qualifyJobResult)
    # $qualifyJob = $qualifyJobScript.BeginInvoke()

    # $buildScriptFolder = Resolve-Path -Path "$PSScriptRoot/../../ui"
    # $buildJobResult = @{ }
    # $buildJobScript = [PowerShell]::Create().AddScript( {
    #         param($buildScriptFolder, $buildJobResult)    
    #         $buildJobResult.path = $buildScriptFolder
    #         Push-Location $buildScriptFolder
            
    #         node .\node_modules\@angular\cli\bin\ng build --prod --no-progress
    #         $buildJobResult.exitCode = $LASTEXITCODE
    #     }).AddArgument($buildScriptFolder).AddArgument($buildJobResult)
    # $buildJob = $buildJobScript.BeginInvoke()

    # Write-Host ""
    # Write-Host "Creating the aws instance..."
    # $instanceJobScript.EndInvoke($instanceJob)
    # if ( $instanceJobResult.exitCode -ne 0 -or $null -eq $instanceJobResult.newHostId) {
    #     throw "Creating the instance failed"
    # }
    # else {
    #     $newHostId = $instanceJobResult.newHostId
    #     Write-Host "Creating the instance $($instanceJobResult.newHostId) successful!"
    # }

    # Write-Host ""
    # Write-Host "Qualifying the build to deploy..."
    # $qualifyJobScript.EndInvoke($qualifyJob)
    # if ( $qualifyJobResult.exitCode -ne 0) {
    #     throw "Qualifying the build failed"
    # }
    # else {
    #     Write-Host "Build is qualified to continue!"
    # }

    # Write-Host ""
    # Write-Host "Building the artifact to deploy..."
    # $buildJobScript.EndInvoke($buildJob)
    # if ( $buildJobResult.exitCode -ne 0) {
    #     Write-Host $buildJobResult.path
    #     Write-Host $buildJobResult.exitCode
    #     throw "Building the artifact failed"
    # }
    # else {
    #     Write-Host "Artifact ready to deploy!"
    # }

    # $copyResults = . "$PSScriptRoot\copyArtifacts.ps1" $newHostId
    # $copyExitCode = $LASTEXITCODE
    # if ( $copyExitCode -ne 0 ) {
    #     Write-Host $copyResults
    #     throw "copy failed"
    # }

    # $newHost = GetInstanceByInstanceId $newHostId
    # . "$PSScriptRoot\runUiTests.ps1" $newHost.PublicIpAddress
    # $e2eExitCode = $LASTEXITCODE
    # if ($e2eExitCode -ne 0) {
    #     throw "e2e failed"
    # }

    # $staleProdId = GetProdInstanceId
    # SwitchHost "$newHostId"
    # TerminateEc2Instance $staleProdId
    # $result = "succeeded"
# }
# catch {
#     if ($null -ne $newHostId) {
#         TerminateEc2Instance $newHostId
#     }
#     $result = "failed"
#     Write-Host $_.Exception.Message
# }

$timer.stop()
$elapsedTime = [math]::Round($timer.elapsedmilliseconds / 1000)

""
Write-Host "Deploy done in $($elapsedTime) seconds."