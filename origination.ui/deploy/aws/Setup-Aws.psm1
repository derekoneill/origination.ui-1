Function SetupAws {
    if ($true -eq $awsSetupComplete) {
        exit
    }
    if (Get-Module -ListAvailable -Name AWS.Tools.EC2) {
        Write-Host "Aws Tool installed"
    } 
    else {
        Install-Module -Name AWS.Tools.EC2 -Force -AllowClobber
    }

    $deployUserName = 'DeployUser'
    $profiles = Get-AWSCredential -ListProfileDetail
    $deployUser = $profiles.where({$_.ProfileName -eq $deployUserName})
    if ($deployUser) {
        Write-Host "Deploy User already set"
    } else {
        # Get this from the user file
        Set-AWSCredential `
            -AccessKey AKIA55SIOTCN4UEFXDN4 `
            -SecretKey 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 `
            -StoreAs $deployUserName
        Initialize-AWSDefaultConfiguration -ProfileName $deployUserName -Region us-east-1

        Write-Host "$deployUserName added to aws credentials"
    }

    $deployKeyName = 'ProgApply'
    $deployKeyFile = "$PSScriptRoot/../aws/${deployKeyName}.pem"

    try {
        Get-EC2KeyPair -KeyName $deployKeyName
        Write-Host "Key Pair already loaded"
        $keyFileExists = Test-Path -Path $deployKeyFile
        if (!$keyFileExists) {
            throw [System.IO.FileNotFoundException] "Missing the .pem file required for connecting to AWS"
        }
    }
    catch {
        $newKeyPair = New-EC2KeyPair -KeyName $deployKeyName
        Write-Host "New Key Pair generated"
        $newKeyPair | Get-Member
        $newKeyPair.KeyMaterial | Out-File -Encoding ascii $deployKeyFile
    }
    $awsSetupComplete = $true;
}

Export-ModuleMember -Function SetupAws