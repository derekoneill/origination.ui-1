using Microsoft.Extensions.Configuration;

namespace origination.ui
{
    public enum ProgEnvironment
    {
        LOCALHOST = 1,
        QA = 2,
        RC = 3,
        DEMO = 4,
        PROD = 5
    }

    public static class ProgEnvironmentExtensions
    {
        public static ProgEnvironment GetProgEnvironment(this IConfiguration self)
        {
            var aspNetEnv = self.GetValue<string>("AppSettings:ENV");

            if (aspNetEnv == "Development")
            {
                return ProgEnvironment.LOCALHOST;
            }
            else if (aspNetEnv.StartsWith("QA"))
            {
                return ProgEnvironment.QA;
            }
            else if (aspNetEnv == "RC")
            {
                return ProgEnvironment.RC;
            }
            else if (aspNetEnv == "DMO")
            {
                return ProgEnvironment.DEMO;
            }
            else if (aspNetEnv == "PRD")
            {
                return ProgEnvironment.PROD;
            }

            return ProgEnvironment.QA;
        }

        public static ProgEnvironment GetProgEnvironment(this IConfigurationBuilder self)
        {
            return self.Build().GetProgEnvironment();
        }
    }
}