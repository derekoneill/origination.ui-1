import { APP_BASE_HREF, PlatformLocation } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {
  APP_INITIALIZER,
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule
} from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgIdleModule } from '@ng-idle/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { GritUniversalModule } from '@progleasing/grit-universal-angular';
import { ApplyModule } from '@ua/apply';
import { DataAccessModule } from '@ua/shared/data-access';
import { AppRoutingModule } from './app-routing.module';
import { AppStartup } from './app-startup';
import { AppComponent } from './app.component';

export const httpLoaderFactory = (http: HttpClient) => new TranslateHttpLoader(http, './assets/translations/');

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    ReactiveFormsModule,
    DataAccessModule,
    AppRoutingModule,
    GritUniversalModule,
    ApplyModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgIdleModule.forRoot(),
  ],
  providers: [
    AppStartup,
    {
      provide: APP_BASE_HREF,
      useFactory: (platformLocation: PlatformLocation) =>
        platformLocation.getBaseHrefFromDOM(),
      deps: [PlatformLocation],
    },
    {
      provide: APP_INITIALIZER,
      useFactory: (appStartup: AppStartup) => () => appStartup.load(),
      deps: [AppStartup, ApplyModule],
      multi: true,
    },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent],
})
export class AppModule { }