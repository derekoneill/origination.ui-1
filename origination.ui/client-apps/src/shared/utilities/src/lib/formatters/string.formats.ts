import moment from 'moment';

export const phoneTextMask = [
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
];

export const dateOfBirthTextMask = [
  /\d/,
  /\d/,
  '/',
  /\d/,
  /\d/,
  '/',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
];
export const ssnTextMask = [
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
];

export const expirationDateTextMask = [/\d/, /\d/, '/', /\d/, /\d/];

export const convertFormDate = (date: string): string =>
{
  const dateParts = date.split('/');
  const newDate = `${dateParts[2]}-${dateParts[0]}-${dateParts[1]}`;
  return newDate;
};

export const formatFormDate = (date: Date | string): string => {
  if (!date) {
    return null;
  }
  let formatDate;
  if (date instanceof Date) {
    if (!date.getFullYear()) {
      return null;
    }
    formatDate = new Date(date);
  } else if (typeof date === 'string') {
    formatDate = moment(date).toDate();
    if (!formatDate.getFullYear()) {
      return null;
    }
  }
  const month = ('00' + (formatDate.getMonth() + 1).toString()).slice(-2);
  const day = ('00' + formatDate.getDate().toString()).slice(-2);
  const year = ('0000' + formatDate.getFullYear()).slice(-4);
  return `${month}/${day}/${year}`;
};