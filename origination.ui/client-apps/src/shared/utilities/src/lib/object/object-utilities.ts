export const deepMerge = <T>(copyFrom: T, updates: T): T => Object.keys(copyFrom).reduce((builder, key) => {
  if (builder[key]) {
    if (copyFrom[key] instanceof Object) {
      builder[key] = deepMerge(copyFrom[key], builder[key]);
    }

    return builder;
  }

  builder[key] = copyFrom[key];
  return builder;
}, updates);