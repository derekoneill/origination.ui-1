export const constants = {
  stateSearch: [
    {
      value: 'AL',
      searchTerms: ['AL', 'Alabama'],
      display: 'Alabama',
    },
    {
      value: 'AK',
      searchTerms: ['AK', 'Alaska'],
      display: 'Alaska',
    },
    {
      value: 'AZ',
      searchTerms: ['AZ', 'Arizona'],
      display: 'Arizona',
    },
    {
      value: 'AR',
      searchTerms: ['AR', 'Arkansas'],
      display: 'Arkansas',
    },
    {
      value: 'CA',
      searchTerms: ['CA', 'California'],
      display: 'California',
    },
    {
      value: 'CO',
      searchTerms: ['CO', 'Colorado'],
      display: 'Colorado',
    },
    {
      value: 'CT',
      searchTerms: ['CT', 'Connecticut'],
      display: 'Connecticut',
    },
    {
      value: 'DE',
      searchTerms: ['DE', 'Delaware'],
      display: 'Delaware',
    },
    {
      value: 'FL',
      searchTerms: ['FL', 'Florida'],
      display: 'Florida',
    },
    {
      value: 'GA',
      searchTerms: ['GA', 'Georgia'],
      display: 'Georgia',
    },
    {
      value: 'HI',
      searchTerms: ['HI', 'Hawaii'],
      display: 'Hawaii',
    },
    {
      value: 'ID',
      searchTerms: ['ID', 'Idaho'],
      display: 'Idaho',
    },
    {
      value: 'IL',
      searchTerms: ['IL', 'Illinois'],
      display: 'Illinois',
    },
    {
      value: 'IN',
      searchTerms: ['IN', 'Indiana'],
      display: 'Indiana',
    },
    {
      value: 'IA',
      searchTerms: ['IA', 'Iowa'],
      display: 'Iowa',
    },
    {
      value: 'KS',
      searchTerms: ['KS', 'Kansas'],
      display: 'Kansas',
    },
    {
      value: 'KY',
      searchTerms: ['KY', 'Kentucky'],
      display: 'Kentucky',
    },
    {
      value: 'LA',
      searchTerms: ['LA', 'Louisiana'],
      display: 'Louisiana',
    },
    {
      value: 'ME',
      searchTerms: ['ME', 'Maine'],
      display: 'Maine',
    },
    {
      value: 'MD',
      searchTerms: ['MD', 'Maryland'],
      display: 'Maryland',
    },
    {
      value: 'MA',
      searchTerms: ['MA', 'Massachusetts'],
      display: 'Massachusetts',
    },
    {
      value: 'MI',
      searchTerms: ['MI', 'Michigan'],
      display: 'Michigan',
    },
    {
      value: 'MN',
      searchTerms: ['MN', 'Minnesota'],
      display: 'Minnesota',
    },
    {
      value: 'MS',
      searchTerms: ['MS', 'Mississippi'],
      display: 'Mississippi',
    },
    {
      value: 'MO',
      searchTerms: ['MO', 'Missouri'],
      display: 'Missouri',
    },
    {
      value: 'MT',
      searchTerms: ['MT', 'Montana'],
      display: 'Montana',
    },
    {
      value: 'NE',
      searchTerms: ['NE', 'Nebraska'],
      display: 'Nebraska',
    },
    {
      value: 'NV',
      searchTerms: ['NV', 'Nevada'],
      display: 'Nevada',
    },
    {
      value: 'NH',
      searchTerms: ['NH', 'New Hampshire'],
      display: 'New Hampshire',
    },
    {
      value: 'NJ',
      searchTerms: ['NJ', 'New Jersey'],
      display: 'New Jersey',
    },
    {
      value: 'NM',
      searchTerms: ['NM', 'New Mexico'],
      display: 'New Mexico',
    },
    {
      value: 'NY',
      searchTerms: ['NY', 'New York'],
      display: 'New York',
    },
    {
      value: 'NC',
      searchTerms: ['NC', 'North Carolina'],
      display: 'North Carolina',
    },
    {
      value: 'ND',
      searchTerms: ['ND', 'North Dakota'],
      display: 'North Dakota',
    },
    {
      value: 'OH',
      searchTerms: ['OH', 'Ohio'],
      display: 'Ohio',
    },
    {
      value: 'OK',
      searchTerms: ['OK', 'Oklahoma'],
      display: 'Oklahoma',
    },
    {
      value: 'OR',
      searchTerms: ['OR', 'Oregon'],
      display: 'Oregon',
    },
    {
      value: 'PA',
      searchTerms: ['PA', 'Pennsylvania'],
      display: 'Pennsylvania',
    },
    {
      value: 'RI',
      searchTerms: ['RI', 'Rhode Island'],
      display: 'Rhode Island',
    },
    {
      value: 'SC',
      searchTerms: ['SC', 'South Carolina'],
      display: 'South Carolina',
    },
    {
      value: 'SD',
      searchTerms: ['SD', 'South Dakota'],
      display: 'South Dakota',
    },
    {
      value: 'TN',
      searchTerms: ['TN', 'Tennessee'],
      display: 'Tennessee',
    },
    {
      value: 'TX',
      searchTerms: ['TX', 'Texas'],
      display: 'Texas',
    },
    {
      value: 'UT',
      searchTerms: ['UT', 'Utah'],
      display: 'Utah',
    },
    {
      value: 'VT',
      searchTerms: ['VT', 'Vermont'],
      display: 'Vermont',
    },
    {
      value: 'VA',
      searchTerms: ['VA', 'Virginia'],
      display: 'Virginia',
    },
    {
      value: 'WA',
      searchTerms: ['WA', 'Washington'],
      display: 'Washington',
    },
    {
      value: 'WV',
      searchTerms: ['WV', 'West Virginia'],
      display: 'West Virginia',
    },
    {
      value: 'WI',
      searchTerms: ['WI', 'Wisconsin'],
      display: 'Wisconsin',
    },
    {
      value: 'WY',
      searchTerms: ['WY', 'Wyoming'],
      display: 'Wyoming',
    },
  ],
};