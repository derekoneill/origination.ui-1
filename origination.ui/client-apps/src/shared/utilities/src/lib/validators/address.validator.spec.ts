import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { LocationService } from '@ua/shared/data-access';
import {
  cityValidator,
  stateValidator,
  street1Validator,
  street2Validator,
  validateStateZipMatch,
  zipValidator,
} from '@ua/shared/utilities';
import { BehaviorSubject, of } from 'rxjs';

describe('Address Validators', () => {
  describe('City validator', () => {
    let mockCityControl: SubstituteOf<FormControl>;

    beforeEach(() => {
      mockCityControl = Substitute.for<FormControl>();
    });

    it('Detects number in city', () => {
      const testCityName = 'City7';
      mockCityControl.value.returns(testCityName);
      const res = cityValidator(mockCityControl);
      expect(res.number).toBe(true);
    });

    it('Detects special character in city', () => {
      const testCityName = 'City%';
      mockCityControl.value.returns(testCityName);
      const res = cityValidator(mockCityControl);
      expect(res.specialChar).toBe(true);
    });

    it('Detects leading space in city', () => {
      const testCityName = ' City';
      mockCityControl.value.returns(testCityName);
      const res = cityValidator(mockCityControl);
      expect(res.spaceFormat).toBe(true);
    });

    it('Legitimate city name passes', () => {
      const testCityName = 'City';
      mockCityControl.value.returns(testCityName);
      const res = cityValidator(mockCityControl);
      expect(res).toBeNull();
    });
  });

  describe('State validator', () => {
    let mockStateControl: SubstituteOf<FormControl>;

    beforeEach(() => {
      mockStateControl = Substitute.for<FormControl>();
    });

    it('No error if empty', () => {
      mockStateControl.value.returns(null);
      const res = stateValidator(mockStateControl);
      expect(res).toBeUndefined();
    });

    it('Detects leading space', () => {
      const testState = ' UT';
      mockStateControl.value.returns(testState);
      const res = stateValidator(mockStateControl);
      expect(res.spaceFormat).toBe(true);
    });

    it('Detects invalid state', () => {
      const testState = 'BQ';
      mockStateControl.value.returns(testState);
      const res = stateValidator(mockStateControl);
      expect(res.selection).toBe(true);
    });

    it('Detects valid state', () => {
      const testState = 'UT';
      mockStateControl.value.returns(testState);
      const res = stateValidator(mockStateControl);
      expect(res).toBeNull();
    });
  });

  describe('Secondary Street validator', () => {
    let mockStreetControl: SubstituteOf<FormControl>;

    beforeEach(() => {
      mockStreetControl = Substitute.for<FormControl>();
    });

    it('Detects leading space', () => {
      const testStreet = ' space street';
      mockStreetControl.value.returns(testStreet);
      const res = street2Validator()(mockStreetControl);
      expect(res.spaceFormat).toBe(true);
    });

    it('Valid street', () => {
      const testStreet = 'Valid Street';
      mockStreetControl.value.returns(testStreet);
      const res = street2Validator()(mockStreetControl);
      expect(res).toBeNull();
    });
  });

  describe('Primary Street validator', () => {
    let mockStreetControl: SubstituteOf<FormControl>;

    beforeEach(() => {
      mockStreetControl = Substitute.for<FormControl>();
    });

    it('Detects PO Box', () => {
      const testStreet = 'PO Box 123';
      mockStreetControl.value.returns(testStreet);
      const res = street1Validator(mockStreetControl);
      expect(res.poBox).toBe(true);
    });

    it('Detect Leading Space', () => {
      const testStreet = ' TestStreet';
      mockStreetControl.value.returns(testStreet);
      const res = street1Validator(mockStreetControl);
      expect(res.spaceFormat).toBe(true);
    });

    it('Detect Invalid Characters', () => {
      const testStreet = '%$82&!';
      mockStreetControl.value.returns(testStreet);
      const res = street1Validator(mockStreetControl);
      expect(res.formatting).toBe(true);
    });

    it('No error on valid street', () => {
      const testStreet = '123 Street';
      mockStreetControl.value.returns(testStreet);
      const res = street1Validator(mockStreetControl);
      expect(res).toBeNull();
    });
  });

  describe('Zip validator', () => {
    let mockZipControl: SubstituteOf<FormControl>;

    beforeEach(() => {
      mockZipControl = Substitute.for<FormControl>();
    });

    it('Detects too few characters', () => {
      const testZip = '12';
      mockZipControl.value.returns(testZip);
      const res = zipValidator(mockZipControl);
      expect(res.sixToEightDigits).toBe(true);
    });

    it('Detects too many characters', () => {
      const testZip = '1291146545';
      mockZipControl.value.returns(testZip);
      const res = zipValidator(mockZipControl);
      expect(res.sixToEightDigits).toBe(true);
    });

    it('Detects valid zip length', () => {
      const testZip = '84045';
      mockZipControl.value.returns(testZip);
      const res = zipValidator(mockZipControl);
      expect(res).toBeNull();
    });
  });

  describe('Validate State and Zip', () => {
    let mockFormGroup: SubstituteOf<FormGroup>;
    let mockLocationService: SubstituteOf<LocationService>;

    beforeEach(() => {
      mockLocationService = Substitute.for<LocationService>();
      mockFormGroup = Substitute.for<FormGroup>();
    });

    it('Valid State and Zip have no error', () => {
      const mockState = 'UT';
      const mockZip = '84045';

      const mockZipField = Substitute.for<AbstractControl>();
      const mockStateField = Substitute.for<AbstractControl>();

      const mockLock = new BehaviorSubject(false);
      mockFormGroup.get('zip').returns(mockZipField);
      mockFormGroup.get('state').returns(mockStateField);
      mockZipField.value.returns(mockZip);
      mockStateField.value.returns(mockState);

      mockLocationService.validateStateAndZip(Arg.any()).returns(of(true));
      const validator = validateStateZipMatch(mockLock, mockLocationService);
      validator(mockFormGroup);

      mockZipField.didNotReceive().setErrors(Arg.any(), Arg.any());
    });

    it('Invalid State and Zip has error', () => {
      const mockState = 'UT';
      const mockZip = '84045';

      const mockZipField = Substitute.for<AbstractControl>();
      const mockStateField = Substitute.for<AbstractControl>();

      const mockLock = new BehaviorSubject(false);
      mockFormGroup.get('zip').returns(mockZipField);
      mockFormGroup.get('state').returns(mockStateField);
      mockZipField.value.returns(mockZip);
      mockStateField.value.returns(mockState);

      mockLocationService.validateStateAndZip(Arg.any()).returns(of(false));
      const validator = validateStateZipMatch(mockLock, mockLocationService);
      validator(mockFormGroup);

      mockZipField.received().setErrors(Arg.any(), Arg.any());
    });
  });
});