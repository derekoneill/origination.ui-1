import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { GritCoreModule } from '@progleasing/grit-core';
import { GritUniversalModule } from '@progleasing/grit-universal-angular';
import { AnalyticsModule } from '@ua/shared/analytics';
import { ExternalHtmlComponent } from './components/external-html/external-html.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { TimeoutPromptComponent } from './components/timeout-prompt/timeout-prompt.component';
import { TranslateModule } from '@ngx-translate/core';
import { UaLayoutComponent } from './components/ua-layout/ua-layout.component';
import { LayoutService } from './layout.service';

@NgModule({
  imports: [
    AnalyticsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    GritCoreModule,
    RouterModule,
    GritUniversalModule,
    TranslateModule
  ],
  providers: [
    LayoutService,
  ],
  entryComponents: [],
  declarations: [
    HeaderComponent,
    UaLayoutComponent,
    FooterComponent,
    TimeoutPromptComponent,
    ExternalHtmlComponent,
  ],
  exports: [
    GritCoreModule,
    GritUniversalModule,
    HeaderComponent,
    FooterComponent,
    TimeoutPromptComponent,
    UaLayoutComponent,
    ExternalHtmlComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ComponentsModule { }