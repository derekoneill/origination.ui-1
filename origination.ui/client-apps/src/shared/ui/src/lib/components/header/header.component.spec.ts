import { EventEmitter } from '@angular/core';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { LayoutElements, LayoutService } from '../../layout.service';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let mockBackEmitter: SubstituteOf<EventEmitter<void>>;
  let mockCloseEmitter: SubstituteOf<EventEmitter<void>>;
  let mockLogoClickEmitter: SubstituteOf<EventEmitter<void>>;
  let mockLayoutService: SubstituteOf<LayoutService>;

  beforeEach(() => {
    mockBackEmitter = Substitute.for<EventEmitter<void>>();
    mockCloseEmitter = Substitute.for<EventEmitter<void>>();
    mockLogoClickEmitter = Substitute.for<EventEmitter<void>>();
    mockLayoutService = Substitute.for<LayoutService>();

    component = new HeaderComponent(mockLayoutService);
    component.back = mockBackEmitter;
    component.closeApp = mockCloseEmitter;
    component.logoClick = mockLogoClickEmitter;
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('on init', () => {
    const layoutElements = new Subject<LayoutElements>();

    beforeEach(() => {
      mockLayoutService.layoutElements$.returns(layoutElements.asObservable());
      component.ngOnInit();
    });

    it('should get the current state of the layout elements', () => {
      const expected = { hasHeaderBackButton: chance.bool(), hasHeaderExitButton: chance.bool() } as LayoutElements;
      let actual: LayoutElements;
      component.layoutElements$.subscribe(el => (actual = el));
      layoutElements.next(expected);
      expect(actual).toBe(expected);
    });
  });

  it('should emit back event when the arrow back icon being clicked.', () => {
    component.previous();

    mockBackEmitter.received().emit();
  });

  it('should emit closeApp event when the close icon being clicked.', () => {
    component.exit();

    mockCloseEmitter.received().emit();
  });

  it('should emit logoClick event when the logo is clicked.', () => {
    component.onLogoClick();

    mockLogoClickEmitter.received().emit();
  });

});