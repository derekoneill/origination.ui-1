import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LayoutElements, LayoutService } from '../../layout.service';

@Component({
  selector: 'ua-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Output() back: EventEmitter<void> = new EventEmitter();
  @Output() closeApp: EventEmitter<void> = new EventEmitter();
  @Output() logoClick: EventEmitter<void> = new EventEmitter();

  public layoutElements$: Observable<LayoutElements>;

  constructor(private readonly layoutService: LayoutService) { }

  public ngOnInit(): void {
    this.layoutElements$ = this.layoutService.layoutElements$;
  }

  public previous(): void {
    this.back.emit();
  }

  public exit(): void {
    this.closeApp.emit();
  }

  public onLogoClick(): void {
    this.logoClick.emit();
  }
}