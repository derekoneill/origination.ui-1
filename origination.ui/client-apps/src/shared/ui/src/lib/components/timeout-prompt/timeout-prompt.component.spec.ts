import { ChangeDetectorRef } from '@angular/core';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { LayoutElements, LayoutService } from '../../layout.service';
import { TimeoutPromptComponent } from './timeout-prompt.component';
import { TranslateService } from '@ngx-translate/core';
describe('TimeoutPromptComponent', () => {
  let component: TimeoutPromptComponent;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockCD: SubstituteOf<ChangeDetectorRef>;
  let mockTranslateService: SubstituteOf<TranslateService>;

  beforeEach(() => {
    mockLayoutService = Substitute.for<LayoutService>();
    mockCD = Substitute.for<ChangeDetectorRef>();
    mockTranslateService = Substitute.for<TranslateService>();

    component = new TimeoutPromptComponent(
      mockLayoutService,
      mockCD,
      mockTranslateService
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    const layoutElements = new Subject<LayoutElements>();
    beforeEach(() => {
      mockLayoutService.layoutElements$.returns(layoutElements.asObservable());
      component.ngOnInit();
    });

    it('should get the current state of the layout elements', () => {
      const expected = { showTimeoutPrompt: chance.bool() } as LayoutElements;
      let actual: LayoutElements;
      component.layoutElements$.subscribe(el => (actual = el));
      layoutElements.next(expected);
      expect(actual).toBe(expected);
    });
  });
});