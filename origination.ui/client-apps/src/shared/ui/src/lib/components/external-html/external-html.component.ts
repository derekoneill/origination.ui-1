import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { AnalyticsService, ClickEventType } from '@ua/shared/analytics';

@Component({
  selector: '[externalHtml], ua-external-html',
  templateUrl: './external-html.component.html',
})
export class ExternalHtmlComponent implements AfterViewInit {
  @Input() externalHtml: string;
  @ViewChild('externalHtmlBlock')
  externalHtmlBlock: ElementRef;

  constructor(private analyticsService: AnalyticsService) { }

  ngAfterViewInit(): void {
    this.anchorAnalytics(this.externalHtmlBlock);
  }

  private anchorAnalytics(htmlContentBlock: ElementRef): void {
    const anchors: HTMLAnchorElement[] = htmlContentBlock.nativeElement.querySelectorAll('a');
    for (const anchor of anchors) {
      anchor.onclick = () => {
        this.analyticsService.trackUniversalLinkClick({
          eventProperties: {
            eventLabel: anchor.innerText,
          },
          type: ClickEventType.link
        });
      };
    }
  }
}