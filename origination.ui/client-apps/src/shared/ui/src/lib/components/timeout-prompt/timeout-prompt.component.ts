import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, timer } from 'rxjs';
import { LayoutElements, LayoutService } from '../../layout.service';
@Component({
  selector: 'ua-timeout-prompt',
  templateUrl: './timeout-prompt.component.html',
  styleUrls: ['./timeout-prompt.component.scss'],
})
export class TimeoutPromptComponent implements OnInit {

  @Output() public interruptIdle: EventEmitter<boolean> = new EventEmitter<boolean>();
  public layoutElements$: Observable<LayoutElements>;
  public baseCountdownText = '';
  public countdownText: string;

  private isVisible = false;
  private promptElement: ElementRef;
  private timeoutTime = 60;
  private timeTilTimeout = this.timeoutTime;
  @ViewChild('timeoutPrompt') set content(content: ElementRef) {
    if (content) { this.promptElement = content; }
  }

  constructor(
    private readonly layoutService: LayoutService,
    private readonly cdRef: ChangeDetectorRef,
    private readonly translateService: TranslateService,
    ) { }

  ngOnInit(): void {
    this.layoutElements$ = this.layoutService.layoutElements$;
    this.translateService.get('modals.timeout.content').subscribe(modalContent => {
      this.registerContent(modalContent);
    });
    timer(1000, 1000).subscribe(() => this.countDown());
  }

  public countDown(): void {
    if (this.promptElement == null) { return; }
    if (!this.isVisible && this.promptElement.nativeElement.offsetParent != null) {
      this.isVisible = true;
    }
    else if (this.isVisible && this.promptElement.nativeElement.offsetParent == null) {
      this.isVisible = false;
    }

    if (!this.isVisible) {
      this.timeTilTimeout = this.timeoutTime;
      return;
    }

    if (this.timeTilTimeout > 0) { this.timeTilTimeout -= 1; }

    this.updateCDText();
  }

  public stay(): void {
  }

  public onInterruptIdle(requestLogout: boolean): void {
    this.interruptIdle.emit(requestLogout);
  }

  private updateCDText() {
    const timeRep = /{{ +seconds +}}/gi;

    this.countdownText = this.baseCountdownText.replace(timeRep, this.timeTilTimeout.toString());
    this.cdRef.detectChanges();
  }

  private registerContent(content: string) {
    this.baseCountdownText = content;
    this.updateCDText();
  }
}