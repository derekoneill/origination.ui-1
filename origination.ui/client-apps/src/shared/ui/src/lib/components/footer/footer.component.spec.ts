import { Renderer2 } from '@angular/core';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { TranslateService } from '@ngx-translate/core';
import { AnalyticsService } from '@ua/shared/analytics';
import { chance } from 'jest-chance';
import { Observable, of, Subject } from 'rxjs';
import { LayoutElements, LayoutService } from '../../layout.service';
import { FooterComponent, LanguageCode } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockRenderer: SubstituteOf<Renderer2>;
  let mockDocument: SubstituteOf<Document>;
  let mockAnalyticsService: SubstituteOf<AnalyticsService>;
  let mockTranslateService: SubstituteOf<TranslateService>;

  let mockLanguageSelector: Observable<LanguageCode>;

  beforeEach(() => {
    mockLayoutService = Substitute.for<LayoutService>();
    mockRenderer = Substitute.for<Renderer2>();
    mockDocument = Substitute.for<Document>();
    mockAnalyticsService = Substitute.for<AnalyticsService>();
    mockTranslateService = Substitute.for<TranslateService>();

    component = new FooterComponent(
      mockLayoutService,
      mockRenderer,
      mockAnalyticsService,
      mockTranslateService,
      mockDocument
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    const layoutElements = new Subject<LayoutElements>();
    beforeEach(() => {
      mockLayoutService.layoutElements$.returns(layoutElements.asObservable());
      component.ngOnInit();
    });

    it('should get the current state of the layout elements', () => {
      const expected = { hasFooter: chance.bool() } as LayoutElements;
      let actual: LayoutElements;
      component.layoutElements$.subscribe(el => (actual = el));
      layoutElements.next(expected);
      expect(actual).toBe(expected);
    });
  });

  describe('presentLocalizationChoice', () => {
    let buttonText: string;

    beforeEach(() => {
      buttonText = chance.string();
      component.presentLocalizationModal = false;
    });

    it('should not present a localization modal if the user is not attempting to change current language', () => {
      mockLanguageSelector = of(LanguageCode.english);
      component.currentLocalization$ = mockLanguageSelector;
      component.presentLocalizationChoice(LanguageCode.english, buttonText);
      expect(component.presentLocalizationModal).toBe(false);
      expect(component.desiredLocalization).toBeUndefined();
    });

    it('should present a localization modal if the user is attempting to change current language', () => {
      mockLanguageSelector = of(LanguageCode.english);
      component.currentLocalization$ = mockLanguageSelector;

      component.presentLocalizationChoice(LanguageCode.spanish, buttonText);
      expect(component.presentLocalizationModal).toBe(true);
      expect(component.desiredLocalization).toBe(LanguageCode.spanish);
    });

    it('should track localization interaction', () => {
      const localization = chance.pickone([LanguageCode.english, LanguageCode.spanish]);
      mockLanguageSelector = of(localization);
      component.currentLocalization$ = mockLanguageSelector;
      component.presentLocalizationChoice(localization, buttonText);

      mockTranslateService.received().instant(buttonText);
      mockAnalyticsService.received().trackLocalizationInteraction(Arg.any(), localization);
    });
  });

  describe('confirmLocalizationChoice', () => {

    beforeEach(() => {
      component.presentLocalizationModal = true;
      component.desiredLocalization = LanguageCode.spanish;
    });

    it('should no longer present the localization modal', () => {
      component.confirmLocalizationChoice();
      expect(component.presentLocalizationModal).toBe(false);
    });
  });

  describe('closeLocalizationOverlay', () => {
    it('should no longer present the localization modal', () => {
      component.presentLocalizationModal = true;
      component.closeLocalizationOverlay();
      expect(component.presentLocalizationModal).toBe(false);
    });
    it('should unset the overflow style on the document body', () => {
      component.presentLocalizationModal = true;
      component.closeLocalizationOverlay();
      mockRenderer
        .received()
        .setStyle(
          mockDocument.body,
          'overflow',
          'unset'
        );
    });
  });
});