import { ElementRef } from '@angular/core';
import { waitForAsync } from '@angular/core/testing';
import Substitute, { Arg, SubstituteOf } from '@fluffy-spoon/substitute';
import { AnalyticsService } from '@ua/shared/analytics';
import { ExternalHtmlComponent } from './external-html.component';
import { chance } from 'jest-chance';

describe('ExternalHtmlComponent', () => {
  let component: ExternalHtmlComponent;
  let mockAnalyticsService: SubstituteOf<AnalyticsService>;
  let mockElementRef: SubstituteOf<ElementRef>;
  let anchor: HTMLAnchorElement;

  beforeEach(waitForAsync(() => {
    mockAnalyticsService = Substitute.for<AnalyticsService>();
    mockAnalyticsService.trackUniversalLinkClick(Arg.any()).returns();
    component = new ExternalHtmlComponent(mockAnalyticsService);
    mockElementRef = Substitute.for<ElementRef>();
    anchor = {
      click: Substitute.for<HTMLAnchorElement>().click().returns,
    } as HTMLAnchorElement;
    mockElementRef.nativeElement.returns({
      innerHtml: chance.string(),
      querySelectorAll: () => ([
        anchor
      ])
    });
    component.externalHtmlBlock = mockElementRef;
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fire an analytics event when a link is clicked', () => {
    component.ngAfterViewInit();
    anchor.onclick(undefined);
    mockAnalyticsService.received().trackUniversalLinkClick(Arg.any());
  });
});