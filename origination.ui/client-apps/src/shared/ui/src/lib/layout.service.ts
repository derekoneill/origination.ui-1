import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  private readonly layoutSubject$ = new BehaviorSubject<LayoutElements>({
    hasFooter: false,
    hasHeaderBackButton: true,
    hasHeaderExitButton: true,
    timeoutEnabled: false,
    showTimeoutPrompt: false,
  } as LayoutElements);

  private editable: boolean;

  constructor() { }

  get layoutElements$() {
    return this.layoutSubject$.asObservable();
  }

  public showFooter(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      hasFooter: true,
    });
  }

  public hideFooter(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      hasFooter: false,
    });
  }

  public showHeaderBackButton(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      hasHeaderBackButton: true,
    });
  }

  public hideHeaderBackButton(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      hasHeaderBackButton: false,
    });
  }

  public showHeaderExitButton(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      hasHeaderExitButton: true,
    });
  }

  public hideHeaderExitButton(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      hasHeaderExitButton: false,
    });
  }

  public showTimeoutPromptButton(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      showTimeoutPrompt: true,
    });
  }

  public hideTimeoutPromptButton(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      showTimeoutPrompt: false,
    });
  }

  public enableTimeout(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      timeoutEnabled: true
    });
  }

  public disableTimeout(): void {
    this.layoutSubject$.next({
      ...this.getSnapshot(),
      timeoutEnabled: false
    });
  }

  public get editMode(): boolean {
    return this.editable;
  }

  public enableEditMode(): void {
    this.editable = true;
  }

  public disableEditMode(): void {
    this.editable = false;
  }

  private getSnapshot(): LayoutElements {
    return this.layoutSubject$.value;
  }
}

export interface LayoutElements {
  hasHeaderBackButton: boolean;
  hasHeaderExitButton: boolean;
  hasFooter: boolean;
  showTimeoutPrompt: boolean;
  timeoutEnabled: boolean;
}