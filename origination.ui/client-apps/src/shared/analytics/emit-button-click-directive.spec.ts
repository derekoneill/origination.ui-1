import { ElementRef } from '@angular/core';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { ClickEventType } from './analytics-event-emission-enums';
import { ClickEventEmission, ClickEventProperties } from './analytics-event-emission-interfaces';
import { AnalyticsService } from './analytics.service';
import { EmitButtonClickDirective } from './emit-button-click-directive';

describe('EmitButtonClickDirective', () => {
    let directive: EmitButtonClickDirective;
    let mockElementRef: SubstituteOf<ElementRef>;
    let mockAnalyticsService: SubstituteOf<AnalyticsService>;
    let innerText: string;

    beforeEach(() => {
        mockElementRef = Substitute.for<ElementRef>();
        mockAnalyticsService = Substitute.for<AnalyticsService>();
        innerText = chance.string();
        mockElementRef.nativeElement.returns({
            innerText,
        });

        directive = new EmitButtonClickDirective(mockElementRef, mockAnalyticsService);
    });

    it('should be created', () => {
        expect(directive).toBeDefined();
    });

    it('should fire a button click analytics event with default click event properties', () => {
        const mockClickEventProperties: ClickEventProperties = {};
        const expected: ClickEventEmission = {
            eventProperties: {
                eventLabel: innerText,
                eventValue: 0
            },
            type: ClickEventType.button
        };

        directive.clickEventProperties = mockClickEventProperties;
        directive.onClick();

        mockAnalyticsService.received().trackUniversalButtonClick(expected);
    });

    it('should fire a button click analytics event with click event property overrides', () => {
        const eventLabel = chance.string();
        const eventValue = chance.integer({ min: 0, max: 1000 });
        const eventType = chance.pickone([ClickEventType.button, ClickEventType.link, ClickEventType.paymentEstimatorButton]);
        const mockClickEventProperties: ClickEventProperties = {
            eventProperties: {
                eventLabel,
                eventValue
            },
            eventType
        };
        const expected: ClickEventEmission = {
            eventProperties: {
                eventLabel,
                eventValue
            },
            type: eventType
        };

        directive.clickEventProperties = mockClickEventProperties;
        directive.onClick();

        mockAnalyticsService.received().trackUniversalButtonClick(expected);
    });

});