import { ElementRef } from '@angular/core';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { ClickEventType } from './analytics-event-emission-enums';
import { AnalyticsEventProperties, ClickEventEmission, ClickEventProperties } from './analytics-event-emission-interfaces';
import { AnalyticsService } from './analytics.service';
import { EmitLinkClickDirective } from './emit-link-click.directive';

describe('EmitLinkClickDirective', () => {
    let directive: EmitLinkClickDirective;
    let mockElementRef: SubstituteOf<ElementRef>;
    let mockAnalyticsService: SubstituteOf<AnalyticsService>;
    let innerText: string;

    beforeEach(() => {
        mockElementRef = Substitute.for<ElementRef>();
        mockAnalyticsService = Substitute.for<AnalyticsService>();
        innerText = chance.string();
        mockElementRef.nativeElement.returns({
            innerText,
        });

        directive = new EmitLinkClickDirective(mockElementRef, mockAnalyticsService);
    });

    it('should be created', () => {
        expect(directive).toBeDefined();
    });

    it('should fire a link click analytics event with default click event properties', () => {
        const mockAnalyticsEventProperties: AnalyticsEventProperties = {} as AnalyticsEventProperties;
        const expected: ClickEventEmission = {
            eventProperties: {
                eventLabel: innerText,
                eventValue: 0
            },
            type: ClickEventType.link
        };

        directive.clickEventProperties = mockAnalyticsEventProperties;
        directive.onClick();

        mockAnalyticsService.received().trackUniversalLinkClick(expected);
    });

    it('should fire a link click analytics event with click event property overrides', () => {
        const eventLabel = chance.string();
        const eventValue = chance.integer({ min: 0, max: 1000 });
        const mockAnalyticsEventProperties: AnalyticsEventProperties = {
            eventLabel,
            eventValue
        };
        const expected: ClickEventEmission = {
            eventProperties: {
                eventLabel,
                eventValue
            },
            type: ClickEventType.link
        };

        directive.clickEventProperties = mockAnalyticsEventProperties;
        directive.onClick();

        mockAnalyticsService.received().trackUniversalLinkClick(expected);
    });

});