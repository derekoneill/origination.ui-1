import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { FormElementEngagementEventType } from './analytics-event-emission-enums';
import { FormElementEventProperties } from './analytics-event-emission-interfaces';
import { AnalyticsService } from './analytics.service';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'input, grit-field, [formElementEngagement]',
})
export class EmitFormElementEngagementDirective {
  @Input('formElementEngagement')
  formElementEngagementProperties: FormElementEventProperties;

  constructor(
    private elementRef: ElementRef,
    private analyticsService: AnalyticsService
  ) { }

  @HostListener('click') onClick(): void {
    if (this.elementRef.nativeElement.tagName === 'INPUT') {
      return;
    }
    const formElementEngagementEventLabel: string =
      this.formElementEngagementProperties &&
        this.formElementEngagementProperties.eventProperties &&
        this.formElementEngagementProperties.eventProperties.eventLabel
        ? this.formElementEngagementProperties.eventProperties.eventLabel
        : this.elementRef.nativeElement.innerText;
    const formElementEngagementEventValue: number =
      this.formElementEngagementProperties &&
        this.formElementEngagementProperties.eventProperties &&
        this.formElementEngagementProperties.eventProperties.eventValue
        ? this.formElementEngagementProperties.eventProperties.eventValue
        : 0;
    const formElementEngagementEventType: FormElementEngagementEventType =
      this.formElementEngagementProperties &&
        this.formElementEngagementProperties.type
        ? this.formElementEngagementProperties.type
        : FormElementEngagementEventType.formElement;
    this.analyticsService.trackFormElementEngagement({
      eventProperties: {
        eventLabel: formElementEngagementEventLabel,
        eventValue: formElementEngagementEventValue,
      },
      type: formElementEngagementEventType,
    });
  }

  @HostListener('blur') onBlur(): void {
    if (
      this.elementRef.nativeElement.tagName !== 'INPUT' ||
      (this.elementRef.nativeElement.tagName === 'INPUT' &&
        !this.elementRef.nativeElement.value)
    ) {
      return;
    }
    const formElementEngagementEventLabel: string =
      this.formElementEngagementProperties &&
        this.formElementEngagementProperties.eventProperties &&
        this.formElementEngagementProperties.eventProperties.eventLabel
        ? this.formElementEngagementProperties.eventProperties.eventLabel
        : this.elementRef.nativeElement.id;
    const formElementEngagementEventValue: number =
      this.formElementEngagementProperties &&
        this.formElementEngagementProperties.eventProperties &&
        this.formElementEngagementProperties.eventProperties.eventValue
        ? this.formElementEngagementProperties.eventProperties.eventValue
        : 0;
    const formElementEngagementEventType: FormElementEngagementEventType =
      this.formElementEngagementProperties &&
        this.formElementEngagementProperties.type
        ? this.formElementEngagementProperties.type
        : FormElementEngagementEventType.formElement;
    this.analyticsService.trackFormFill({
      eventProperties: {
        eventLabel: formElementEngagementEventLabel,
        eventValue: formElementEngagementEventValue,
      },
      type: formElementEngagementEventType,
    });
  }
}