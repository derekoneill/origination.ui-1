import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class StoreLocatorService {
  constructor(private http: HttpClient) { }

  public getStoreLocationData(storeId: number): Observable<StoreLocationData>
  {
    return this.http.get<StoreLocationData>(`store/store?storeId=${storeId}`);
  }
}

export interface StoreLocationData {
  name: string;
  isEcommerce: boolean;
  address: StoreAddress;
}

export interface StoreAddress {
  address1: string;
  address2?: string;
  city: string;
  state: string;
  zipCode: string;
  country: string;
  latitude: number;
  longitude: number;
}