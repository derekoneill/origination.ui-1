import { HttpClient, HttpResponse } from '@angular/common/http';
import { Substitute, Arg, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { AppConfig } from '../config';
import { StoreLocatorService } from './store-locator.service';

describe('StoreLocatorService', () => {
  let service: StoreLocatorService;

  let mockHttpClient: SubstituteOf<HttpClient>;

  beforeEach(() => {
    mockHttpClient = Substitute.for<HttpClient>();

    service = new StoreLocatorService(mockHttpClient);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('Get Store Location Data', () => {
    let storeIdSubject: Subject<HttpResponse<string>>;

    beforeEach(() => {
      storeIdSubject = new Subject<HttpResponse<string>>();
      mockHttpClient.get(Arg.any()).returns(storeIdSubject.asObservable());
    });

    it('should get store location data', () => {
      const storeId = chance.integer();
      let actual;

      service.getStoreLocationData(storeId).subscribe(x => {
        actual = x;
      });

      const content = chance.string();
      storeIdSubject.next(content);

      mockHttpClient
        .received()
        .get(`store/store?storeId=${storeId}`);
      expect(actual).toBe(content);
    });
  });
});