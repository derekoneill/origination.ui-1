import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConfig } from '../config';
import { ValidateZipStateRequest, ZipCodeLookupResponse } from '../models';

@Injectable()
export class LocationService {
  constructor(private http: HttpClient, private appConfig: AppConfig) { }

  validateStateAndZip(
    validateZipStateReq: ValidateZipStateRequest
  ): Observable<boolean> {
    if (
      !validateZipStateReq ||
      !validateZipStateReq.zipCode ||
      !validateZipStateReq.stateCode ||
      validateZipStateReq.zipCode.length !== 5
    ) {
      return of(false);
    }

    return this.http
      .post<ZipCodeLookupResponse>(
        `${this.appConfig.serviceConfigs.ecom}/v1/locations/validate`,
        validateZipStateReq
      )
      .pipe(map(res => res.isValidStateForZip));
  }
}