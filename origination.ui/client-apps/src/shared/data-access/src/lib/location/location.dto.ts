export interface ValidateZipStateRequest {
  zipCode: string;
  stateCode: string;
}

export interface ZipCodeLookupResponse {
  isValidStateForZip: boolean;
}