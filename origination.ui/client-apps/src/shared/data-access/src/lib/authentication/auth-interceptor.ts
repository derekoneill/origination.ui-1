import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthState } from './auth-state';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  public constructor(private readonly authState: AuthState) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.authState.jwt.pipe(take(1)).subscribe(token => {
      if (token) {
        req = req.clone({
          setHeaders: {
            authorization: `Bearer ${token}`,
          },
        });
      }
    });
    return next.handle(req);
  }
}