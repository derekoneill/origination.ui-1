export interface AuthCustomer {
  customerId: number;
  accessToken: string;
  firstName: string;
  lastName: string;
  onboardingGuid: string;
  storeId: number;
  language: string;
  birthdate: string;
  email: string;
  mobilePhone: string;
  ssn: string;
}