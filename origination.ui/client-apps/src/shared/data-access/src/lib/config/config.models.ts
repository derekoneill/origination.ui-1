export interface ServiceConfigs {
  // existing apply flow.
  ecom: string;
  auth: string;
  ui: string;
  leasePaymentEstimator: string;
  leasePaymentTool: string;
}