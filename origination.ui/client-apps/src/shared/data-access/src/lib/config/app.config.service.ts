import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ServiceConfigs } from './config.models';

@Injectable()
export class AppConfig {
  public serviceConfigs: ServiceConfigs = {} as ServiceConfigs;

  constructor(private http: HttpClient) { }

  load() {
    const configServicesUrl = 'api/configs/services';
    return this.http
      .get<ServiceConfigs>(configServicesUrl)
      .pipe(
        tap((serviceConfigs) => {
          this.serviceConfigs = serviceConfigs;
        })
      )
      .toPromise();
  }
}