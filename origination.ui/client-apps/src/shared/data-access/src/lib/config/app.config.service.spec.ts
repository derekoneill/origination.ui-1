import { HttpClient, HttpResponse } from '@angular/common/http';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { AppConfig } from './app.config.service';

describe('App Config Service', () => {
  let service: AppConfig;

  let mockHttp: SubstituteOf<HttpClient>;

  beforeEach(() => {
    mockHttp = Substitute.for<HttpClient>();

    service = new AppConfig(mockHttp);
  });

  describe('load', () => {
    let httpResponse: Subject<HttpResponse<string>>;

    beforeEach(() => {
      httpResponse = new Subject<HttpResponse<string>>();
      mockHttp.get(Arg.any()).returns(httpResponse);
    });
    it('should send the get request', () => {
      const expectedUrl = 'api/configs/services';
      service.load();

      mockHttp.received().get(expectedUrl);
    });

    it('should set the settings', () => {
      const body = chance.string();
      const response = { body } as HttpResponse<string>;

      service.load();
      httpResponse.next(response);

      expect(service.serviceConfigs).toBe(response);
    });
  });
});