export enum ApplyPageKey {
  accountInfo = 'account-info',
  bankInfo = 'bank-info',
  contactInfo = 'home-address',
  incomeInfo = 'income-info',
  initialPayment = 'card-info',
  landingPage = 'get-started',
  leaseCostEstimator = 'estimator',
  loginResume = 'login-resume',
  notFound = 'error',
  results = 'results',
  review = 'review',
  somethingBroke = 'something-broke',
  submitApplicationTerms = 'submit-app',
  submittedApplication = 'processing-app',
  termsAndConditions = 'terms-and-conditions',
  prefill = 'prefill'
}