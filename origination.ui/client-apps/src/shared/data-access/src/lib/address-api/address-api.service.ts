import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfig } from '../config';
import { ApiZipLookupDto } from './api-zip-lookup.dto';

@Injectable()
export class AddressApiService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  getCityAndStateByZip(zip: string): Observable<ApiZipLookupDto> {
    const uri = `api/addressLookup?zip=${zip}`;

    return this.http.get<ApiZipLookupDto>(uri);
  }
}