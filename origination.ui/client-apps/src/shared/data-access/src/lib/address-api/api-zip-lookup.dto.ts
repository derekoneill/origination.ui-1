export interface ApiZipLookupDto {
  zip: string;
  city: string;
  state: string;
}