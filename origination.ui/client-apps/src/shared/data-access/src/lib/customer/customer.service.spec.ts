import { HttpClient } from '@angular/common/http';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Customer } from '.';
import { CreditCardInformation } from './customer-model';
import { CustomerService } from './customer.service';

describe('Customer Service', () => {
  let customerService: CustomerService;

  let mockHttp: SubstituteOf<HttpClient>;

  beforeEach(() => {
    mockHttp = Substitute.for<HttpClient>();

    customerService = new CustomerService(mockHttp);
  });

  describe('Update Customer', () => {
    let customer: Customer;

    beforeEach(() => {
      customer = {} as Customer;
    });

    it('should PATCH to the origination api', () => {
      customerService.updateCustomer(customer);

      mockHttp.received().patch('customer/api/v2/customer', Arg.any());
    });
  });

  describe('Get Customer Status', () => {
    let customerId: number;

    beforeEach(() => {
      customerId = chance.integer();
    });

    it('should send the request', () => {
      customerService.getCustomerStatus(customerId);

      mockHttp.received().get(`origination/api/customer/status?customerId=${customerId}`);
    });
  });

  describe('Save Credit Card', () => {
    let creditCard: CreditCardInformation;

    beforeEach(() => {
      creditCard = {} as CreditCardInformation;
    });

    it('should PATCH to the origination api', () => {
      customerService.saveCreditCard(creditCard);

      mockHttp.received().post('origination/api/creditCard', Arg.any());
    });
  });
});