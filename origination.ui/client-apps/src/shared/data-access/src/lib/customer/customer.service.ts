import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer } from '.';
import { CreditCardInformation, CustomerStatus, SaveCreditCardResponse } from './customer-model';

export class CustomerEnvelope {
  public value: Customer;
}

export class CustomerAcknowledgement {

  public customerId: number;
  public acknowledgementType: string;
  public originatingPlatform: string;
  public languageCode: string;
  public statusDateTime: string;
}
@Injectable()
export class CustomerService {
  constructor(private http: HttpClient) { }

  public updateCustomer(customer: Customer): void {
    this.http.patch<void>(`customer/api/v2/customer`, customer).subscribe();
  }

  public createCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(`customer/api/v2/customer`, customer);
  }

  public getCustomerStatus(customerId: number): Observable<CustomerStatus> {
    return this.http.get<CustomerStatus>(`origination/api/customer/status?customerId=${customerId}`);
  }

  public getCustomerData(customerId: number): Observable<CustomerEnvelope>
  {
    return this.http.get<CustomerEnvelope>(`customer/api/customer?customerId=${customerId}`);
  }

  public saveCreditCard(creditCard: CreditCardInformation): Observable<SaveCreditCardResponse> {
    return this.http.post<SaveCreditCardResponse>('origination/api/creditCard', creditCard);
  }

  public sendAcknowledgement(request: CustomerAcknowledgement): void {
    this.http.put<CustomerAcknowledgement>('comprehension/api/Acknowledgement', request).subscribe();
  }
}