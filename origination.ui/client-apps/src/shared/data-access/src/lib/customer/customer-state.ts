import { Injectable } from '@angular/core';
import { deepMerge } from '@ua/shared/utilities';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApplicationPayFrequency } from '../application';
import { Address, BankInformation, CreditCardInformation, Customer, CustomerStatus, IncomeInformation } from '../customer/customer-model';
import { CustomerAcknowledgement, CustomerService } from '../customer/customer.service';
import { DatePipe } from '@angular/common';

@Injectable()
export class CustomerState {
  private readonly stateSubject = new BehaviorSubject<Customer>({
    address: { },
    bank: { },
    incomeSource: { },
  } as Customer);
  private readonly cardSubject = new BehaviorSubject<CreditCardInformation>({ } as CreditCardInformation);

  public constructor(private readonly customerService: CustomerService,
    private readonly datePipe: DatePipe)
  { }

  public updateCustomer(customer: Customer) {
    const updatedCustomer = deepMerge(
      this.getCustomerSnapshot(),
      customer
    );
    this.stateSubject.next(updatedCustomer);
  }

  public updateAndSubmitCustomer(customer: Customer) {
    const updatedCustomer = deepMerge(
      this.getCustomerSnapshot(),
      customer
    );
    this.customerService.updateCustomer(updatedCustomer);
    this.stateSubject.next(updatedCustomer);
  }

  public updateMarketingOptIn(marketingOptIn: boolean) {
    this.updateCustomer({
      ...this.getCustomerSnapshot(),
      marketingOptIn,
    });
  }

  // 'statusDateTime': '2021-07-08T19:36:51.636Z'
  public sendAcknowledgement(languageCode: string) {
    const dateString = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
    this.customerService.sendAcknowledgement(({
      customerId: this.getCustomerSnapshot().customerId,
      acknowledgementType: 'TermsAndConditions',
      originatingPlatform: 'ProgApply',
      languageCode,
      machineName: '',
      statusDateTime: dateString
    } as CustomerAcknowledgement));
  }

  public updateBankInformation(bankInformationUpdates: BankInformation) {
    const updatedBankInfo = {
      ...this.getCustomerSnapshot().bank,
      ...bankInformationUpdates,
    };

    this.updateAndSubmitCustomer({
      ...this.getCustomerSnapshot(),
      bank: updatedBankInfo,
    });
  }

  public updateIncome(incomeUpdates: IncomeInformation) {
    const updatedIncome = {
      ...this.getCustomerSnapshot().incomeSource,
      ...incomeUpdates,
    };

    this.updateAndSubmitCustomer({
      ...this.getCustomerSnapshot(),
      incomeSource: updatedIncome,
    });
  }

  public createCustomer(customer: Customer, languageCode: string) {
    const newCustomer = deepMerge(
      this.getCustomerSnapshot(),
      customer
    );
    this.customerService.createCustomer(newCustomer).subscribe(cust => {
      this.updateCustomer({
        ...this.getCustomerSnapshot(),
        customerId: cust.customerId
      });
      this.sendAcknowledgement(languageCode);
    });
  }
  public updateAddress(addressUpdates: Address) {
    const updatedAddress = {
      ...this.getCustomerSnapshot().address,
      ...addressUpdates,
    };

    this.updateAndSubmitCustomer({
      ...this.getCustomerSnapshot(),
      address: updatedAddress,
    });
  }

  public get customer$(): Observable<Customer> {
    return this.stateSubject.asObservable();
  }

  public get customerStatus$(): Observable<CustomerStatus> {
    return this.customerService.getCustomerStatus(this.getCustomerSnapshot().customerId);
  }

  public get address$(): Observable<Address> {
    return this.customer$.pipe(map(customer => customer.address));
  }

  public get bank$(): Observable<BankInformation> {
    return this.customer$.pipe(map(customer => customer.bank));
  }

  public get income$(): Observable<IncomeInformation> {
    return this.customer$.pipe(map(customer => customer.incomeSource));
  }

  public async submitCreditCard(): Promise<boolean> {
    const creditCard = this.getCardSnapshot();

    return (await this.customerService.saveCreditCard(creditCard).toPromise().then(cardResponse => {
      if (!cardResponse.bin || !cardResponse.token || cardResponse.customerId === null) { return false; }

      creditCard.bin = cardResponse.bin;
      creditCard.token = cardResponse.token;

      this.updateCreditCard(creditCard);
      return true;
    }).catch(error => {
      creditCard.token = '';
      creditCard.bin = '';
      this.updateCreditCard(creditCard);
      return false;
    }));
  }

  public updateCreditCard(creditCard: CreditCardInformation) {
    this.cardSubject.next(creditCard);
  }

  public get card$(): Observable<CreditCardInformation> {
    return this.cardSubject.asObservable();
  }

  public loadCustomerInfo(customerId: number): void {
    this.customerService.getCustomerData(customerId).subscribe(custData => {
      const customer = custData.value;
      this.removeEmptyValues(customer);
      const updatedCustomer = deepMerge(
        this.getCustomerSnapshot(),
        customer
      );
      this.stateSubject.next(updatedCustomer);
    });
  }

  private getCustomerSnapshot(): Customer {
    return this.stateSubject.value;
  }

  private getCardSnapshot(): CreditCardInformation {
    return this.cardSubject.value;
  }

  private removeEmptyValues(obj: Customer | Record<string, unknown>): void {
    Object.keys(obj).forEach(key => {
      if (obj.hasOwnProperty(key)) {
        const value: Record<string, unknown> = obj[key];
        if (value === null || value === undefined) {
          delete obj[key];
        }
        else if (typeof value === 'object') {
          this.removeEmptyValues(value);
          obj[key] = value;
        }
      }
    });
  }
}