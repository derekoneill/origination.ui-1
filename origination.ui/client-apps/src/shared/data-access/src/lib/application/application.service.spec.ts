import { HttpClient } from '@angular/common/http';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { Subject } from 'rxjs';
import {
  Application,
  ApplicationService,
  Decision
} from '../index';

describe('ApplicationService', () => {
  let applicationService: ApplicationService;
  let mockHttp: SubstituteOf<HttpClient>;

  beforeEach(() => {
    mockHttp = Substitute.for<HttpClient>();
    applicationService = new ApplicationService(mockHttp);
  });

  describe('Submit Origination Application', () => {
    let application: Application;

    beforeEach(() => {
      application = {} as Application;
    });

    it('should post to the origination api', () => {
      applicationService.submitApplication(application);

      mockHttp.received().post(`origination/api/unifiedApplication/submit`, Arg.any());
    });

    it('should post the application', () => {
      applicationService.submitApplication(application);

      mockHttp.received().post(Arg.any(), application);
    });

    it('should return the response', () => {
      const submitSubject = new Subject<Decision>();
      mockHttp.post(Arg.any(), Arg.any()).returns(submitSubject.asObservable());
      let actual: Decision;
      const expected = {} as Decision;

      applicationService
        .submitApplication(application)
        .subscribe(response => (actual = response));
      submitSubject.next(expected);

      expect(actual).toBe(expected);
    });
  });
});