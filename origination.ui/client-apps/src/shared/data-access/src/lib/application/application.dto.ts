export interface Application {
  storeId: number;
  customerId: number;
}

export enum PayFrequency {
  weekly = 0,
  everyOtherWeek = 1,
  twicePerMonth = 2,
  monthly = 3,
}

export enum ApplicationPayFrequency {
  weekly = 'Every week',
  everyOtherWeek = 'Every other week',
  twicePerMonth = 'Twice a month',
  monthly = 'Monthly'
}

export enum DecisionStatus {
  approved = 'Approved',
  preApproved = 'Pre-Approved',
  pending = 'Pending',
  inProcess = 'In Process',
  denied = 'Denied',
  error = 'Error',
}

export interface Decision {
  leaseId: number;
  status: DecisionStatus;
  approvalLimit: number;
}