import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Application, Decision } from '.';

@Injectable()
export class ApplicationService {
  constructor(private http: HttpClient) { }

  public requestPrefill(customerId: number, storeId: number, lastFour: string): Observable<Application> {
    return this.http.get<Application>(
      `origination/api/application/prefill&customerId=${customerId}&storeId=${storeId}&lastFour=${lastFour}`);
  }


  public submitApplication(
    application: Application
  ): Observable<Decision> {
    return this.http
      .post<Decision>(
        `origination/api/unifiedApplication/submit`,
        application
      )
      .pipe(map(response => response));
  }
}