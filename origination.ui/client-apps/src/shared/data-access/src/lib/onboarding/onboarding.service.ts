import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConfig } from '../config';
import { Onboarding } from './onboarding-state';

export interface OnboardingInfoRequest {
  onboardingGuid: string;
}

export interface OnboardingInfo {
  onboardingGuid: string;
  emailAddress: string;
  languageCode: string;
  originationPlatform: string;
  phoneNumber: string;
  storeId: number;
}
@Injectable({
  providedIn: 'root',
})
export class OnboardingService {
  constructor(private http: HttpClient, private appConfig: AppConfig) { }

  getOnboardingInfo(onboardingGuid: string): Observable<Onboarding> {
    const uri = `origination/api/CustomerOnboarding?onboardingGuid=${onboardingGuid}`;
    return this.http.get<Onboarding>(uri);
  }

  updateLanguage(onboardingGuid: string, languageCode: string): void {
    const uri = `origination/api/CustomerOnboarding`;
    const payload = {
      onboardingGuid,
      languageCode
    };
    this.http.patch<[]>(uri, payload).toPromise();
  }
}