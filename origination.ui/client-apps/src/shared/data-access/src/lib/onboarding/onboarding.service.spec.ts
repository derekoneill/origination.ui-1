import { HttpClient, HttpResponse } from '@angular/common/http';
import { Substitute, Arg, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { AppConfig } from '../config';
import { OnboardingService } from './onboarding.service';

describe('OnboardingService', () => {
  let service: OnboardingService;

  let mockHttpClient: SubstituteOf<HttpClient>;
  let mockAppConfig: SubstituteOf<AppConfig>;

  beforeEach(() => {
    mockHttpClient = Substitute.for<HttpClient>();
    mockAppConfig = Substitute.for<AppConfig>();

    service = new OnboardingService(mockHttpClient, mockAppConfig);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('Update language', () => {
    it('Should update language', () => {
      const tempGuid = chance.string();
      const languageCode = chance.string();
      const payload = {
        onboardingGuid: tempGuid,
        languageCode
      };
      service.updateLanguage(tempGuid, languageCode);
      mockHttpClient
        .received()
        .patch(`origination/api/CustomerOnboarding`, payload);
    });
  });

  describe('Get Content', () => {
    let onboardingSubject: Subject<HttpResponse<string>>;

    beforeEach(() => {
      onboardingSubject = new Subject<HttpResponse<string>>();
      mockHttpClient.get(Arg.any()).returns(onboardingSubject.asObservable());
    });

    it('should get content', () => {
      const tempGuid = chance.string();

      let actual;
      service.getOnboardingInfo(tempGuid).subscribe(x => {
        actual = x;
      });
      const content = chance.string();
      onboardingSubject.next(content);

      mockHttpClient
        .received()
        .get(`origination/api/CustomerOnboarding?onboardingGuid=${tempGuid}`);
      expect(actual).toBe(content);
    });
  });
});