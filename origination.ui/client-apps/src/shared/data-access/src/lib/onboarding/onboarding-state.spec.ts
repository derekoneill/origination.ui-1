import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { StoreLocatorService, StoreLocationData } from '../store-locator';
import { Onboarding, OnboardingState } from './onboarding-state';
import { OnboardingService } from './onboarding.service';

describe('Onboarding State', () => {
  let state: OnboardingState;

  let mockOnboardingService: SubstituteOf<OnboardingService>;
  let mockStoreLocatorService: SubstituteOf<StoreLocatorService>;

  beforeEach(() => {
    mockOnboardingService = Substitute.for<OnboardingService>();
    mockStoreLocatorService = Substitute.for<StoreLocatorService>();

    state = new OnboardingState(mockOnboardingService, mockStoreLocatorService);
  });

  it('should create', () => {
    expect(state).toBeDefined();
  });

  describe('Request Onboarding State', () => {
    let suppliedGuid: string;
    let onboardingSubject: Subject<Onboarding>;
    let storeLocatorSubject: Subject<StoreLocationData>;

    beforeEach(() => {
      suppliedGuid = chance.string();
      onboardingSubject = new Subject<Onboarding>();
      mockOnboardingService
        .getOnboardingInfo(Arg.any())
        .returns(onboardingSubject.asObservable());
      storeLocatorSubject = new Subject<StoreLocationData>();
      mockStoreLocatorService
        .getStoreLocationData(Arg.any())
        .returns(storeLocatorSubject.asObservable());
    });

    it('should make the request to get the onboarding info', () => {
      state.requestOnboardingInfo(suppliedGuid);

      mockOnboardingService.received().getOnboardingInfo(suppliedGuid);
    });

    it('should update the onboarding info', () => {
      let actual: Onboarding;
      const expectedStoreData = {} as StoreLocationData;
      const expectedOnboarding = {
        storeLocation: expectedStoreData
      } as Onboarding;
      state.onboardingInfo$.subscribe(returned => (actual = returned));

      state.requestOnboardingInfo(suppliedGuid);
      onboardingSubject.next(expectedOnboarding);
      storeLocatorSubject.next(expectedStoreData);

      expect(actual).toStrictEqual(expectedOnboarding);
    });

    it('should update the onboarding guid', () => {
      let actual: string;
      const expected = chance.string();
      state.onboardingGuid$.subscribe(returnedGuid => (actual = returnedGuid));

      state.requestOnboardingInfo(suppliedGuid);
      onboardingSubject.next({ onboardingGuid: expected } as Onboarding);
      storeLocatorSubject.next({ } as StoreLocationData);

      expect(actual).toBe(expected);
    });

    it('should update the onboarding language', () => {
      let actual: string;
      const expected = chance.string();
      state.languageCode$.subscribe(
        returnedLanguage => (actual = returnedLanguage)
      );

      state.requestOnboardingInfo(suppliedGuid);
      onboardingSubject.next({ languageCode: expected } as Onboarding);
      storeLocatorSubject.next({ } as StoreLocationData);

      expect(actual).toBe(expected);
    });
  });
});