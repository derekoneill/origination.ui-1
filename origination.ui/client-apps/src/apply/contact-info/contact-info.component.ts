import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Address,
  AddressApiService,
  ApiZipLookupDto,
  Application,
  ApplicationState,
  ApplyPageKey,
  CustomerState,
  LocationService,
  Customer
} from '@ua/shared/data-access';
import {
  cityValidator,
  constants,
  stateValidator,
  street1Validator,
  validateStateZipMatch,
  zipValidator
} from '@ua/shared/utilities';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { LayoutService } from '@ua/shared/ui';

@Component({
  selector: 'ua-contact-info',
  templateUrl: './contact-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactInfoComponent implements OnInit, OnDestroy, AfterViewInit {
  subs: Subscription[] = [];

  contactInfoFormGroup: FormGroup;
  application: Application;
  address: Address;
  pageRoute = ApplyPageKey.contactInfo;

  zipCheckLoadingSubject = new BehaviorSubject(false);
  stateOptions = constants.stateSearch;
  updateZip = new Subject();

  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly applicationState: ApplicationState,
    private readonly customerState: CustomerState,
    private readonly formBuilder: FormBuilder,
    private readonly locationService: LocationService,
    private readonly addressApi: AddressApiService,
    private readonly layoutService: LayoutService
  ) { }

  static checkPageComplete(customer: Customer): boolean {
    return !!(customer.address
              && customer.address.address1
              && customer.address.city
              && customer.address.state
              && customer.address.zip);
  }

  ngOnInit() {
    this.subs.push(
      this.applicationState.application$.subscribe(
        application => (this.application = application)
      ),
      this.customerState.address$.subscribe(
        address => (this.address = address)
      )
    );

    this.contactInfoFormGroup = this.formBuilder.group(
      {
        street1: [
          this.address.address1,
          [Validators.required, street1Validator],
        ],
        street2: [this.address.address2, [street1Validator]],
        city: [this.address.city, [Validators.required, cityValidator]],
        state: [
          this.address.state ?? '',
          [Validators.required, stateValidator],
        ],
        zip: [this.address.zip ?? '', [Validators.required, zipValidator]],
      },
      {
        validator: validateStateZipMatch(
          this.zipCheckLoadingSubject,
          this.locationService
        ),
      }
    );
  }

  getZipStateMismatchError(baseError: string) {
    if (!baseError) {
      return;
    }
    const zipRep = /{{ +zip +}}/gi;
    const stateRep = /{{ +state +}}/gi;
    const newError = baseError
      .replace(stateRep, this.contactInfoFormGroup.get('state').value)
      .replace(zipRep, this.contactInfoFormGroup.get('zip').value);
    return newError;
  }

  ngAfterViewInit() {
    this.layoutService.enableTimeout();
    this.subs.push(
      this.contactInfoFormGroup.get('state').valueChanges.subscribe(x => {
        if (!x) {
          // Remove zip validation if its there. Since its tied to this control
          const zipControl = this.contactInfoFormGroup.get('zip');
          zipControl.setErrors({});
          return;
        }
      }),

      this.contactInfoFormGroup.get('zip').valueChanges.subscribe(x => {
        if (!x || (x.length !== 5 && x.replace('-', '').length !== 9)) {
          return;
        }
        this.updateCityAndStateOnZip();
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(s => s.unsubscribe());
  }

  onContinue() {
    if (this.contactInfoFormGroup.valid) {
      const address: Address = {
        address1: this.contactInfoFormGroup.get('street1').value,
        address2: this.contactInfoFormGroup.get('street2').value,
        city: this.contactInfoFormGroup.get('city').value,
        state: this.contactInfoFormGroup.get('state').value,
        zip: this.contactInfoFormGroup.get('zip').value,
      };

      this.customerState.updateAddress(address);
      if (!this.layoutService.editMode) {
        this.applyRouter.stepForward();
      } else {
        this.applyRouter.navigate(ApplyPageKey.review);
      }
    }
  }

  private updateCityAndStateOnZip() {
    if (
      this.contactInfoFormGroup.get('city').value &&
      this.contactInfoFormGroup.get('state').value
    ) {
      return;
    }
    this.subs.push(
      this.addressApi
        .getCityAndStateByZip(this.contactInfoFormGroup.get('zip').value)
        .subscribe((cityAndState: ApiZipLookupDto) => {
          if (!this.contactInfoFormGroup.get('city').value) {
            this.contactInfoFormGroup.get('city').patchValue(
              cityAndState.city
            );
          }

          if (!this.contactInfoFormGroup.get('state').value) {
            this.contactInfoFormGroup.get('state').setValue(cityAndState.state);
          }
        })
    );
  }
}