import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppConfig, ApplyPageKey, CustomerState, OnboardingState } from '@ua/shared/data-access';
import { AccountInfoComponent } from './account-info/account-info.component';
import { BankInfoComponent } from './bank-info/bank-info.component';
import { ContactInfoComponent } from './contact-info/contact-info.component';
import { IncomeInfoComponent } from './income-info/income-info.component';
import { InitialPaymentComponent } from './initial-payment/initial-payment.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { PrefillComponent } from './prefill/prefill.component';

@Injectable()
export class ApplyRouter {
  public static readonly window: Window = window;
  public constructor(
    private readonly angularRoute: ActivatedRoute,
    private readonly angularRouter: Router,
    private readonly config: AppConfig,
    private readonly location: Location,
    private readonly onboardingState: OnboardingState,
    private readonly customerState: CustomerState,
  ) { }

  public stepBack(): void {
    this.location.back();
  }

  public stepForward(): void {
    const nextPage = routesOrder[this.getCurrentPage()].nextRoute;
    if (!nextPage) {
      throw Error(
        'Attempted to route forward without knowing what page comes next'
      );
    }
    this.navigate(nextPage);
  }

  public navigate(pageKey: ApplyPageKey, params?: Params) {
    if (params) {
      this.angularRouter.navigate([`/${pageKey}`], {
        queryParamsHandling: 'merge',
        queryParams: params,
      });
      return;
    }
    this.angularRouter.navigate([`/${pageKey}`]);
  }

  public externalNavigate(url: string) {
    ApplyRouter.window.location.href = url;
  }

  public forwardToAuth() {
    this.onboardingState.onboardingGuid$.subscribe(guid => {
      this.externalNavigate(
        `${this.config.serviceConfigs.auth
        }/auth?onboarding=${guid}&onSuccess=${this.config.serviceConfigs.ui
        }/${ApplyPageKey.termsAndConditions}&onCancel=${this.config.serviceConfigs.ui
        }/${ApplyPageKey.landingPage}`
      );
    });
  }

  public restartApplication() {
    this.onboardingState.onboardingGuid$.subscribe(guid => {
      const url = ApplyRouter.window.location.href;
      ApplyRouter.window.location.href = url.replace(/\/[^\/]*$/, `/${ApplyPageKey.landingPage}?onboardingGuid=${guid}`);
    });
  }

  public getParam(paramKey: string) {
    return this.angularRoute.snapshot.queryParamMap.get(paramKey);
  }

  public navToFirstIncompletePage(): void {
    this.customerState.customer$.subscribe(customer => {
      let page = routesOrder[ApplyPageKey.accountInfo];
      let pageRoute = ApplyPageKey.accountInfo;

      do {
        pageRoute = page.nextRoute;

        if (pageRoute === undefined) {
          break;
        }

        page = routesOrder[pageRoute];

        if (page.component !== undefined) {
          if (page.component.hasOwnProperty('checkPageComplete') && page.component['checkPageComplete'](customer)) {
              continue;
          }
          this.navigate(pageRoute);
          return;
        }
      } while (page !== undefined);
      this.navigate(ApplyPageKey.review);
    });
  }

  private getCurrentPage(): ApplyPageKey {
    const currentSegments = this.angularRouter.url.split('/');
    const currentPageSegment = currentSegments[
      currentSegments.length - 1
    ].split('?')[0];
    const pageKey = Object.keys(ApplyPageKey).find(
      x => ApplyPageKey[x] === currentPageSegment
    );
    return ApplyPageKey[pageKey];
  }
}

export const routesOrder: {
  [key in ApplyPageKey]?: { nextRoute: ApplyPageKey; component: unknown };
} = {
  [ApplyPageKey.landingPage]: {
    nextRoute: ApplyPageKey.termsAndConditions,
    component: undefined
  },
  [ApplyPageKey.leaseCostEstimator]: {
    nextRoute: ApplyPageKey.termsAndConditions,
    component: undefined
  },
  [ApplyPageKey.notFound]: {
    nextRoute: undefined,
    component: undefined
  },
  [ApplyPageKey.review]: {
    nextRoute: ApplyPageKey.submitApplicationTerms,
    component: undefined
  },
  [ApplyPageKey.termsAndConditions]: {
    nextRoute: ApplyPageKey.accountInfo,
    component: TermsAndConditionsComponent
  },
  [ApplyPageKey.prefill]: {
    nextRoute: ApplyPageKey.accountInfo,
    component: PrefillComponent
  },
  [ApplyPageKey.accountInfo]: {
    nextRoute: ApplyPageKey.contactInfo,
    component: AccountInfoComponent
  },
  [ApplyPageKey.contactInfo]: {
    nextRoute: ApplyPageKey.incomeInfo,
    component: ContactInfoComponent
  },
  [ApplyPageKey.incomeInfo]: {
    nextRoute: ApplyPageKey.bankInfo,
    component: IncomeInfoComponent
  },
  [ApplyPageKey.bankInfo]: {
    nextRoute: ApplyPageKey.initialPayment,
    component: BankInfoComponent
  },
  [ApplyPageKey.initialPayment]: {
    nextRoute: ApplyPageKey.review,
    component: InitialPaymentComponent
  },
  [ApplyPageKey.review]: {
    nextRoute: ApplyPageKey.submitApplicationTerms,
    component: undefined
  },
  [ApplyPageKey.submitApplicationTerms]: {
    nextRoute: ApplyPageKey.submittedApplication,
    component: undefined
  },
  [ApplyPageKey.submittedApplication]: {
    nextRoute: ApplyPageKey.results,
    component: undefined
  },
  [ApplyPageKey.results]: {
    nextRoute: undefined, // next logical step closes the app
    component: undefined
  },
};