import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Application,
  ApplicationState, ApplyPageKey, BankInformation,
  Customer,
  CustomerState
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { routingNumber } from '@ua/shared/utilities';
import { Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
export enum BankInfoFormFields {
  bankRoutingNumber = 'bankRoutingNumber',
  bankAccountNumber = 'bankAccountNumber',
}

@Component({
  selector: 'ua-bank-info',
  templateUrl: './bank-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BankInfoComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  bankInfoForm: FormGroup;
  isMasked = true;
  application: Application;
  bankInfo: BankInformation;
  pageRoute = ApplyPageKey.bankInfo;

  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly applicationState: ApplicationState,
    private readonly customerState: CustomerState,
    private readonly formBuilder: FormBuilder,
    private readonly layoutService: LayoutService,
  ) { }

  static checkPageComplete(customer: Customer): boolean {
    return !!(customer.bank
      && customer.bank.routingNumber
      && customer.bank.accountNumber);
  }

  public ngOnInit(): void {
    this.layoutService.enableTimeout();

    this.subs.push(
      this.applicationState.application$.subscribe(
        application => (this.application = application)
      ),
      this.customerState.bank$.subscribe(
        bankInfo => (this.bankInfo = bankInfo)
      )
    );

    this.bankInfoForm = this.formBuilder.group({
      bankRoutingNumber: [
        this.bankInfo.routingNumber,
        [Validators.required, routingNumber()],
      ],
      bankAccountNumber: [
        this.bankInfo.accountNumber,
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
    });
  }

  public ngOnDestroy(): void {
    this.subs.forEach(s => !s.closed && s.unsubscribe());
  }

  public onMaskChanged(isMasked: boolean): void {
    this.isMasked = isMasked;
  }

  public onContinue(): void {
    if (this.bankInfoForm.valid) {
      this.customerState.updateBankInformation({
        routingNumber: this.bankInfoForm.get(
          BankInfoFormFields.bankRoutingNumber
        ).value,
        accountNumber: this.bankInfoForm.get(
          BankInfoFormFields.bankAccountNumber
        ).value,
      });
      if (!this.layoutService.editMode) {
        this.applyRouter.stepForward();
      } else {
        this.applyRouter.navigate(ApplyPageKey.review);
      }
    }
  }

  public disallowNonDigitInput(keyPressEvent): boolean {
    const charCode = keyPressEvent.charCode || keyPressEvent.keyCode;
    const newChar = String.fromCharCode(charCode);
    const testRegex = /[^0-9]/g;
    if (testRegex.test(newChar)) {
      return false;
    }
  }
}