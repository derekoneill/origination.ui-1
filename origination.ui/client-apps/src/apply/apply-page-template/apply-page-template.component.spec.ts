import { EventEmitter } from '@angular/core';
import { waitForAsync } from '@angular/core/testing';
import { AbstractControl, FormGroup } from '@angular/forms';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { ApplyPageKey } from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { ApplyPageTemplateComponent } from './apply-page-template.component';

describe('ApplyPageTemplateComponent', () => {
  let component: ApplyPageTemplateComponent;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockContinueEmitter: SubstituteOf<EventEmitter<void>>;
  let mockFormGroup: SubstituteOf<FormGroup>;
  let mockAbstractControlEmpty: SubstituteOf<AbstractControl>;
  let mockAbstractControlFull: SubstituteOf<AbstractControl>;
  const formStatusChanges = new Subject<string>();

  beforeEach(waitForAsync(() => {
    mockLayoutService = Substitute.for<LayoutService>();
    mockContinueEmitter = Substitute.for<EventEmitter<void>>();
    mockAbstractControlEmpty = Substitute.for<AbstractControl>();
    mockAbstractControlEmpty.value.returns(null);
    mockAbstractControlFull = Substitute.for<AbstractControl>();
    mockAbstractControlFull.value.returns(chance.string());
    mockFormGroup = Substitute.for<FormGroup>();
    mockFormGroup.statusChanges.returns(formStatusChanges.asObservable());
    mockFormGroup.controls.returns({
      empty: mockAbstractControlEmpty,
      full: mockAbstractControlFull
    });

    component = new ApplyPageTemplateComponent(mockLayoutService);

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on init', () => {
    let pageKey: ApplyPageKey;
    let editMode: boolean;

    beforeEach(() => {
      pageKey = chance.pickone([
        ApplyPageKey.contactInfo,
        ApplyPageKey.incomeInfo,
        ApplyPageKey.bankInfo
      ]);
      editMode = chance.bool();
      mockLayoutService.editMode.returns(editMode);
      component.pageRoute = pageKey;
      component.formGroup = mockFormGroup;
      component.ngOnInit();
    });

    it('should mark populated controls as touched', () => {
      mockAbstractControlFull.received().markAsTouched();
    });

    it('should not mark empty controls as touched', () => {
      mockAbstractControlEmpty.didNotReceive().markAsTouched();
    });

    it('should determine whether or not the apply page is in edit mode', () => {
      component.editMode = editMode;
    });

    it('should hide the footer', () => {
      mockLayoutService.received().hideFooter();
    });

    it('should show the back button', () => {
      mockLayoutService.received().showHeaderBackButton();
    });

    it('should reflect loading state based on form changes', () => {
      component.loading = true;
      formStatusChanges.next('INVALID');
      expect(component.loading).toBe(false);
    });

    it('should hide the exit button', () => {
      mockLayoutService.received().showHeaderExitButton();
    });
  });

  describe('onContinue', () => {
    beforeEach(() => {
      component.loading = false;
      component.continue = mockContinueEmitter;
      component.onContinue();
    });

    it('should emit continue event', () => {
      mockContinueEmitter.received().emit();
    });

    it('should indicate page loading ux on button', () => {
      expect(component.loading).toBe(true);
    });
  });

  describe('update state', () => {
    beforeEach(() => {
      component.loading = true;
    });

    it('should set loading to false for a status of INVALID', () => {
      const status = 'INVALID';
      component.updateStatus(status);
      expect(component.loading).toBe(false);
    });

    it('should not meddle with loading with a status other than invalid', () => {
      const status = chance.string();
      component.updateStatus(status);
      expect(component.loading).toBe(true);
    });
  });
});