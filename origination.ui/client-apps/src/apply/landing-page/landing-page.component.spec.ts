import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { AnalyticsService } from '@ua/shared/analytics';
import {
  Application,
  ApplicationState,
  ApplyPageKey,
  OnboardingState
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { LandingPageComponent } from './landing-page.component';

describe('LandingPageComponent', () => {
  let component: LandingPageComponent;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockAnalyticsService: SubstituteOf<AnalyticsService>;

  beforeEach(async () => {
    mockApplicationState = Substitute.for<ApplicationState>();
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockOnboardingState = Substitute.for<OnboardingState>();
    mockLayoutService = Substitute.for<LayoutService>();
    mockAnalyticsService = Substitute.for<AnalyticsService>();

    component = new LandingPageComponent(
      mockApplicationState,
      mockApplyRouter,
      mockOnboardingState,
      mockLayoutService,
      mockAnalyticsService,
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('On Init', () => {
    let mockApplicationSubject: Subject<Application>;
    let mockOnboardingSubject: Subject<string>;

    beforeEach(() => {
      mockApplicationSubject = new Subject<Application>();
      mockApplicationState.application$.returns(mockApplicationSubject.asObservable());
      mockOnboardingSubject = new Subject<string>();
      mockOnboardingState.onboardingGuid$.returns(mockOnboardingSubject.asObservable());
      component.ngOnInit();
    });

    it('should set application based on the application state', () => {
      const expected = {} as Application;
      mockApplicationSubject.next(expected);

      expect(component.application).toBe(expected);
    });

    it('should set onboarding based on the application state', () => {
      const expected = chance.string();
      mockOnboardingSubject.next(expected);

      expect(component.onboardingGuid).toBe(expected);
    });

    it('should hide the back button', () => {
      mockLayoutService.received().hideHeaderBackButton();
    });

    it('should hide the exit button', () => {
      mockLayoutService.received().hideHeaderExitButton();
    });

    it('should show the footer', () => {
      mockLayoutService.received().showFooter();
    });

    it('should track an application start event', () => {
      mockAnalyticsService.received().trackApplicationStart();
    });
  });

  describe('Move to next', () => {
    it('should move to next page', () => {
      const onboardingGuid = chance.guid();
      component.onboardingGuid = onboardingGuid;
      component.onContinue();
      mockApplyRouter.received().stepForward();
    });
  });

  describe('moveToPaymentEstimator', () => {
    it('should dispatch an action to move to payment estimation ', () => {
      component.moveToPaymentEstimator();

      mockApplyRouter.received().navigate(ApplyPageKey.leaseCostEstimator);
    });
  });
});