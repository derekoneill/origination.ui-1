import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '@ua/shared/analytics';
import {
  Application,
  ApplicationState,
  ApplyPageKey,
  OnboardingState,
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';

@Component({
  selector: 'ua-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  subs: Subscription[] = [];
  onboardingGuid: string;
  application: Application;

  constructor(
    private readonly applicationState: ApplicationState,
    private readonly applyRouter: ApplyRouter,
    private readonly onboardingState: OnboardingState,
    private readonly layoutService: LayoutService,
    private readonly analyticsService: AnalyticsService,
  ) { }

  ngOnInit(): void {
    this.layoutService.disableTimeout();
    this.subs.push(
      this.applicationState.application$.subscribe(
        application => (this.application = application)
      ),
      this.onboardingState.onboardingGuid$.subscribe(
        onboarding => (this.onboardingGuid = onboarding)
      )
    );
    this.layoutService.hideHeaderBackButton();
    this.layoutService.hideHeaderExitButton();
    this.layoutService.showFooter();
    this.analyticsService.trackApplicationStart();
  }

  onContinue(): void {
    this.applyRouter.stepForward();
  }

  moveToPaymentEstimator(): void {
    this.applyRouter.navigate(ApplyPageKey.leaseCostEstimator);
  }
}