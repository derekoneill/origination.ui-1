import { CurrencyPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DropDownItem } from '@progleasing/grit-core';
import {
  Application,
  ApplicationPayFrequency,
  ApplicationState, ApplyPageKey,
  Customer,
  CustomerState,
  IncomeInformation,
  PayFrequency
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import {
  convertFormDate, formatFormDate,

  validateMonthlyGrossIncome
} from '@ua/shared/utilities';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
interface DropDownItemWithPayFrequency extends DropDownItem {
  enum: PayFrequency;
}

interface PayDateMinMaxRule {
  minDate: Date;
  maxDate: Date;
}

enum IncomeInfoFormField {
  monthlyIncome = 'monthlyIncome',
  lastPayDate = 'lastPayDate',
  nextPayDate = 'nextPayDate',
  paymentFrequency = 'paymentFrequency',
}

@Component({
  selector: 'ua-income-info',
  templateUrl: './income-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IncomeInfoComponent implements OnInit, OnDestroy {
  public subs: Subscription[] = [];
  public incomeForm: FormGroup;
  public application: Application;
  public income: IncomeInformation;
  public paymentOptions: DropDownItemWithPayFrequency[];
  public lastPayDateMinMaxRule: PayDateMinMaxRule;
  public nextPayDateMinMaxRule: PayDateMinMaxRule;
  public pageRoute = ApplyPageKey.incomeInfo;
  public paymentFrequencyElementHasBeenFocused = false;
  public paymentFrequencyElementHasFocus = false;
  public incomeFormField = IncomeInfoFormField;

  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly applicationState: ApplicationState,
    private readonly customerState: CustomerState,
    private readonly translateService: TranslateService,
    private readonly formBuilder: FormBuilder,
    private readonly currencyPipe: CurrencyPipe,
    private readonly layoutService: LayoutService
  ) {
    this.lastPayDateMinMaxRule = this.nextPayDateMinMaxRule = {
      minDate: undefined,
      maxDate: undefined,
    };
  }

  static checkPageComplete(customer: Customer): boolean {
    return !!(customer.incomeSource
      && (customer.incomeSource.monthlyGrossIncome && customer.incomeSource.monthlyGrossIncome !== 0)
      && customer.incomeSource.lastPaymentDate
      && customer.incomeSource.nextPaymentDate
      && customer.incomeSource.payFrequency !== undefined);
  }

  public ngOnInit(): void {
    this.layoutService.enableTimeout();

    this.translateService.get('pages.incomeInformation.form.paymentFrequency.options')
      .subscribe(payOptions => this.buildPaymentFrequencyOptions(payOptions));
    this.subs.push(
      this.applicationState.application$.subscribe(application => {
        this.application = application;
      }),
      this.customerState.income$.subscribe(income => {
        this.income = income;
      })
    );

    this.initForm();
    this.calculateMinAndMaxPayDateRule();
  }

  public showFrequencyHint(): boolean {
    return (
      this.incomeForm.get(this.incomeFormField.paymentFrequency).value &&
      (this.incomeForm.get(this.incomeFormField.paymentFrequency).value ===
        this.paymentOptions[1].value ||
        this.incomeForm.get(this.incomeFormField.paymentFrequency).value ===
        this.paymentOptions[2].value)
    );
  }

  public ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  public formatMoney(value: string): void {
    if (value) {
      this.incomeForm
        .get(IncomeInfoFormField.monthlyIncome)
        .setValue(this.currencyPipe.transform(value, 'USD', '', '1.2-2'));
    }
  }

  public getFormValue(field: IncomeInfoFormField): string {
    return this.incomeForm.get(field).value;
  }

  public onContinue(): void {
    if (this.incomeForm.valid) {
      const mgi = this.getFormValue(IncomeInfoFormField.monthlyIncome).replace(
        /,/gi,
        ''
      );

      this.customerState.updateIncome({
        monthlyGrossIncome: parseFloat(mgi),
        lastPaymentDate: convertFormDate(this.getFormValue(IncomeInfoFormField.lastPayDate)),
        nextPaymentDate: convertFormDate(this.getFormValue(IncomeInfoFormField.nextPayDate)),
        payFrequency: this.getFormValue(
          IncomeInfoFormField.paymentFrequency
        ) as ApplicationPayFrequency,
      });

      if (!this.layoutService.editMode) {
        this.applyRouter.stepForward();
      } else {
        this.applyRouter.navigate(ApplyPageKey.review);
      }
    }
  }

  public disallowNonDigitInput(keyPressEvent): boolean {
    const charCode = keyPressEvent.charCode || keyPressEvent.keyCode;
    const newChar = String.fromCharCode(charCode);

    if (newChar.match(/[.]/)) {
      //Only allow one per!
      const existing = this.getFormValue(IncomeInfoFormField.monthlyIncome);
      if (existing.match(/[.]/)) { return false; }
    }

    const testRegex = /[^0-9.]/g;
    if (testRegex.test(newChar)) {
      return false;
    }
  }

  public paymentFrequencySelected(value: string): void {
    this.incomeForm.get(IncomeInfoFormField.paymentFrequency).setValue(value);
  }

  public paymentFrequencyFocused(): void {
    this.paymentFrequencyElementHasBeenFocused = true;
    this.paymentFrequencyElementHasFocus = true;
  }

  public paymentFrequencyBlurred(): void {
    this.paymentFrequencyElementHasFocus = false;
  }

  private buildPaymentFrequencyOptions(payOptionsText: any): void {
    this.paymentOptions = [];

    for (const payOption in PayFrequency) {
      if (isNaN(Number(payOption))) {
        const item: DropDownItemWithPayFrequency = {
          id: `${payOption}`,
          value: payOptionsText.hasOwnProperty(payOption)
            ? payOptionsText[payOption]
            : payOption,
          displayText: payOptionsText.hasOwnProperty(payOption)
            ? payOptionsText[payOption]
            : payOption,
          searchableTerms: [],
          enum: PayFrequency[payOption as keyof typeof PayFrequency],
        };

        this.paymentOptions.push(item);
      }
    }
  }

  private calculateMinAndMaxPayDateRule(): void {
    let minDate = new Date();
    let maxDate = new Date();
    const today = new Date();

    minDate = new Date(minDate.setDate(minDate.getDate() - 35));
    maxDate = new Date(maxDate.setDate(new Date().getDate() + 35));
    this.lastPayDateMinMaxRule = {
      minDate,
      maxDate: today,
    };

    const nextMinDate = today;
    if (this.incomeForm) {
      const selectedDate = this.getFormValue(IncomeInfoFormField.lastPayDate);
      if (selectedDate) {
        const selected = moment(new Date(selectedDate)).format('MM DD YYYY');
        const now = moment().format('MM DD YYYY');
        if (selected === now) {
          nextMinDate.setDate(today.getDate() + 1);
        }
      }
    }

    this.nextPayDateMinMaxRule = {
      minDate: nextMinDate,
      maxDate,
    };
  }

  private initForm(): void {
    const monthlyIncome: string =
      this.income?.monthlyGrossIncome > 0
        ? this.income.monthlyGrossIncome.toString()
        : '';
    const lastPayDate: string = formatFormDate(this.income.lastPaymentDate);
    const nextPayDate: string = formatFormDate(this.income.nextPaymentDate);
    const paymentFrequency = this.income.payFrequency;

    this.incomeForm = this.formBuilder.group({
      monthlyIncome: [monthlyIncome, validateMonthlyGrossIncome()],
      lastPayDate: [lastPayDate, Validators.required],
      nextPayDate: [nextPayDate, Validators.required],
      paymentFrequency: [paymentFrequency, Validators.required],
    });
  }
}