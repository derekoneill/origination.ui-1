import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
  ApplicationPayFrequency,
  ApplyPageKey,
  CreditCardInformation,
  Customer,
  CustomerState,
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { Observable, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';

@Component({
  selector: 'ua-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  content$: Observable<any>;
  customer: Customer;
  card: CreditCardInformation;
  routes: typeof ApplyPageKey = ApplyPageKey;

  constructor(
    private readonly layoutService: LayoutService,
    private readonly customerState: CustomerState,
    private readonly applyRouter: ApplyRouter,
  ) { }

  ngOnInit(): void {
    this.layoutService.disableEditMode();

    this.subs.push(this.customerState.customer$.subscribe(customer => {
      this.customer = customer;
    }));
    this.subs.push(this.customerState.card$.subscribe(card => this.card = card));

    this.layoutService.hideFooter();
    this.layoutService.showHeaderBackButton();
    this.layoutService.showHeaderExitButton();
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => !s.closed && s.unsubscribe());
  }

  public onContinue(): void {
    this.applyRouter.stepForward();
  }

  public updateInformation(route: ApplyPageKey): void {
    this.layoutService.enableEditMode();
    this.applyRouter.navigate(route);
  }

  public isObjectEmpty(obj: any): boolean {
    return Object.keys(obj).length === 0;
  }
}