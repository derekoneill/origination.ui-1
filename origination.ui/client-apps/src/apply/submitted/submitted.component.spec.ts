import { Router } from '@angular/router';
import Substitute, { SubstituteOf } from '@fluffy-spoon/substitute';
import {
  Application,
  ApplicationState,
  Decision
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { chance } from 'jest-chance';
import { Subject, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { SubmittedComponent } from './submitted.component';

describe('SubmittedComponent', () => {
  let component: SubmittedComponent;
  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockRouter: SubstituteOf<Router>;
  let mockLayoutService: SubstituteOf<LayoutService>;

  beforeEach(() => {
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockRouter = Substitute.for<Router>();
    mockLayoutService = Substitute.for<LayoutService>();

    component = new SubmittedComponent(
      mockApplyRouter,
      mockApplicationState,
      mockLayoutService
    );
  });

  it('should be created', () => {
    expect(component).toBeDefined();
  });

  describe('on init', () => {
    const mockApplication = new Subject<Application>();

    beforeEach(() => {
      mockApplicationState.application$.returns(mockApplication.asObservable());
      component.ngOnInit();
    });

    it('should use the application from the application state', () => {
      const expected = Substitute.for<Application>();

      mockApplication.next(expected);

      expect(component.application).toBe(expected);
    });

    it('should hide the footer', () => {
      mockLayoutService.received().hideFooter();
    });
    it('should hide the back button', () => {
      mockLayoutService.received().hideHeaderBackButton();
    });
    it('should show the exit button', () => {
      mockLayoutService.received().showHeaderExitButton();
    });
  });

  describe('after view init', () => {
    const url = chance.url();
    const mockApplication = Substitute.for<Application>();
    const mockApplicationResult = new Subject<Decision>();

    beforeEach(() => {
      mockRouter.url.returns(url);
      mockApplicationState.decision$.returns(
        mockApplicationResult.asObservable()
      );
      component.application = mockApplication;
      component.ngAfterViewInit();
    });

    it('should submit the application', () => {
      mockApplicationState.received().submitApplication();
    });

    it('should handle decision', () => {
      const mockResult = {
        leaseId: chance.integer(),
      } as Decision;
      spyOn(component, 'handleSubmissionResults');
      mockApplicationResult.next(mockResult);
      expect(component.handleSubmissionResults).toHaveBeenCalledWith(
        mockResult
      );
    });
  });

  describe('on destroy', () => {
    let mockSubscription: SubstituteOf<Subscription>;

    beforeEach(() => {
      mockSubscription = Substitute.for<Subscription>();
      component.subs.push(mockSubscription);
    });

    it('should unsubscribe from all subscriptions', () => {
      component.ngOnDestroy();
      mockSubscription.received().unsubscribe();
    });
  });

  describe('handle submission results', () => {
    let mockAppResult: SubstituteOf<Decision>;

    beforeEach(() => {
      mockAppResult = Substitute.for<Decision>();
    });

    it('should return if application id is not valid', () => {
      mockAppResult.leaseId.returns(
        chance.integer({ min: -100, max: 0 })
      );
      component.handleSubmissionResults(mockAppResult);
      mockApplyRouter.didNotReceive().stepForward();
    });

    it('should step forward if a valid application id is received', () => {
      mockAppResult.leaseId.returns(chance.integer({ min: 1, max: 100 }));
      component.handleSubmissionResults(mockAppResult);
      mockApplyRouter.received().stepForward();
    });
  });
});