import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { TranslateService } from '@ngx-translate/core';
import {
  ApplyPageKey,

  Customer, CustomerState,

  OnboardingState
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { TermsAndConditionsComponent, TermsAndConditionsFormFields } from './terms-and-conditions.component';

describe('BankInfo Component', () => {
  let component: TermsAndConditionsComponent;

  let mockFormBuilder: SubstituteOf<FormBuilder>;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockCustomerState: SubstituteOf<CustomerState>;
  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockTranslateService: SubstituteOf<TranslateService>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;
  beforeEach(() => {
    mockFormBuilder = Substitute.for<FormBuilder>();
    mockLayoutService = Substitute.for<LayoutService>();
    mockCustomerState = Substitute.for<CustomerState>();
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockTranslateService = Substitute.for<TranslateService>();
    mockOnboardingState = Substitute.for<OnboardingState>();

    component = new TermsAndConditionsComponent(
      mockFormBuilder,
      mockLayoutService,
      mockCustomerState,
      mockApplyRouter,
      mockTranslateService,
      mockOnboardingState
    );
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('On Init', () => {
    let mockFormGroup: SubstituteOf<FormGroup>;
    let customerSubject: BehaviorSubject<Customer>;

    beforeEach(() => {
      customerSubject = new BehaviorSubject<Customer>({} as Customer);
      mockCustomerState.customer$.returns(customerSubject.asObservable());

      mockFormGroup = Substitute.for<FormGroup>();
      mockFormBuilder.group(Arg.any()).returns(mockFormGroup);
    });

    it('should keep track of subscriptions', () => {
      expect(component.subs.length).toBe(0);
      component.ngOnInit();

      expect(component.subs.length).toBeGreaterThan(0);
    });

    it('should add validations for the ToC acceptance field', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          console.log(`Group received ${x} and the toc is ${x.tocAgree} and then + ${x.tocAgree[1]}`);
          const validators = x.tocAgree[1];
          return validators.includes(Validators.requiredTrue);
        })
      );
    });

    it('should enable timout feature', () => {
      component.ngOnInit();
      mockLayoutService.received().enableTimeout();
    });

  });

  describe('Subscription cleanup', () => {
    let mockSubscription: SubstituteOf<Subscription>;

    beforeEach(() => {
      const observable = Substitute.for<Observable<any>>();
      mockSubscription = Substitute.for<Subscription>();
      observable.subscribe().returns(mockSubscription);
      component.subs.push(observable.subscribe());
    });

    it('should unsubscribe the open subs', () => {
      mockSubscription.closed.returns(false);
      component.ngOnDestroy();

      mockSubscription.received().unsubscribe();
    });

    it('should not unsubscribe the closed subs', () => {
      mockSubscription.closed.returns(true);
      component.ngOnDestroy();

      mockSubscription.didNotReceive().unsubscribe();
    });
  });

  describe('Continue', () => {
    let mockFormGroup: SubstituteOf<FormGroup>;
    const langCode = 'en';

    beforeEach(() => {
      mockFormGroup = Substitute.for<FormGroup>();
      component.tocForm = mockFormGroup;
    });

    describe('given a valid form', () => {
      beforeEach(() => {
        mockFormGroup.valid.returns(true);
      });

      it('should update the state', () => {
        const tocAgree = true;
        const marketingOptIn = true;

        const mockMarketingField = Substitute.for<AbstractControl>();
        mockMarketingField.value.returns(marketingOptIn);

        mockFormGroup
          .get(TermsAndConditionsFormFields.tocAgree)
          .returns({ value: tocAgree } as AbstractControl);

        mockFormGroup.get(TermsAndConditionsFormFields.marketingOptIn)
          .returns(mockMarketingField);

        component.onContinue();
        mockCustomerState.received().updateMarketingOptIn(marketingOptIn);
      });

      it('should update the state with default marketing opt in value', () => {
        const tocAgree = true;
        const mockMarketingField = Substitute.for<AbstractControl>();
        mockMarketingField.value.returns(false);


        mockFormGroup
          .get(TermsAndConditionsFormFields.tocAgree)
          .returns({ value: tocAgree } as AbstractControl);
        mockFormGroup
          .get(TermsAndConditionsFormFields.marketingOptIn)
          .returns(mockMarketingField);

        component.onContinue();
        mockCustomerState.received().updateMarketingOptIn(false);
      });

      it('should navigate to the next page if not in edit mode', () => {
        mockLayoutService.editMode.returns(false);
        component.onContinue();
        mockApplyRouter.received().stepForward();
      });

      it('should navigate to the review page if in edit mode', () => {
        mockLayoutService.editMode.returns(true);
        component.onContinue();
        mockApplyRouter.received().navigate(ApplyPageKey.review);
      });
    });

    describe('given an invalid form', () => {
      beforeEach(() => {
        mockFormGroup.valid.returns(false);
      });

      it('should not update the state', () => {
        component.onContinue();
        mockCustomerState.didNotReceive().updateMarketingOptIn(Arg.any());
      });
    });
  });
});