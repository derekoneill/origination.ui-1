import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import {
  ApplyPageKey,

  Customer, CustomerState,

  OnboardingState
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';

export enum TermsAndConditionsFormFields {
  tocAgree = 'tocAgree',
  marketingOptIn = 'marketingOptIn',
}

@Component({
  selector: 'ua-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss'],
})
export class TermsAndConditionsComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  pageRoute: ApplyPageKey = ApplyPageKey.termsAndConditions;

  tocForm: FormGroup;
  tocAgreed: boolean;
  marketingOptIn: boolean;
  tocTranslation: string;
  marketingTranslation: string;

  tocBodyKey = 'pages.termsAndConditions.tocBody';
  marketingBodyKey = 'pages.termsAndConditions.marketingOptIn';

  phoneNumber: string;

  constructor(
    private formBuilder: FormBuilder,
    private readonly layoutService: LayoutService,
    private readonly customerState: CustomerState,
    private readonly applyRouter: ApplyRouter,
    private readonly translateService: TranslateService,
    private readonly onboardingState: OnboardingState
  ) { }

  ngOnInit(): void {
    this.layoutService.enableTimeout();
    this.initForm();
    this.subs.push(this.customerState.customer$.subscribe(cust => this.updateCustomer(cust)),
      this.onboardingState.phoneNumber$.subscribe(phone => this.updateMarketingText(phone)));
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => !s.closed && s.unsubscribe());
  }

  public onContinue(): void {
    if (this.tocForm.valid) {
      //this.customerState.sendAcknowledgement(this.languageCode);// Part of the post!
      this.customerState.updateMarketingOptIn(this.tocForm.get(TermsAndConditionsFormFields.marketingOptIn).value ?? false);

      if (!this.layoutService.editMode) {
        this.applyRouter.stepForward();
      } else {
        this.applyRouter.navigate(ApplyPageKey.review);
      }
    }
  }

  private initForm(): void {
    this.tocForm = this.formBuilder.group({
      [TermsAndConditionsFormFields.tocAgree]: [false, [Validators.requiredTrue]],
      [TermsAndConditionsFormFields.marketingOptIn]: [false]
    });

    this.translateService.get(this.tocBodyKey).subscribe(translation => this.tocTranslation = translation);
  }

  private updateMarketingText(phoneNumber: string): void {
    this.translateService.get(this.marketingBodyKey, { phoneNumber: this.formatPhone(phoneNumber) })
      .subscribe(translation => this.marketingTranslation = translation);
  }

  private updateCustomer(customer: Customer): void {
    this.tocForm.get(TermsAndConditionsFormFields.marketingOptIn).setValue(customer.marketingOptIn);
  }

  private formatPhone(phoneNumber: string): string {
    const cleaned = ('' + phoneNumber).replace(/\D/g, '');
    const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      return match[1] + '.' + match[2] + '.' + match[3];
    }
    return null;
  }
}