import { Injectable } from '@angular/core';
import { ClientEventBus } from '@progleasing/client-event-bus';
import {
  AppLoadResult,
  MicroFrontendLoader
} from '@progleasing/micro-frontend-loader';
import { AppConfig } from '@ua/shared/data-access';

@Injectable()
export class LeaseCostEstimatorInjectable {
  static readonly leasePaymentToolSelector = 'lease-payment-tool';

  // This will eventually point to production once the correct version is released

  constructor(private readonly appConfig: AppConfig) { }

  public getClientEventBus(): ClientEventBus {
    return ClientEventBus.getInstance();
  }

  public async loadMicroFrontend(
    targetElement: HTMLElement
  ): Promise<AppLoadResult> {
    return await MicroFrontendLoader.loadApp(
      this.appConfig.serviceConfigs.leasePaymentTool,
      LeaseCostEstimatorInjectable.leasePaymentToolSelector,
      targetElement,
      {}
    );
  }
}