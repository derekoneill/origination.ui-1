import { ElementRef, Renderer2 } from '@angular/core';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { AppLoadResult } from '@progleasing/micro-frontend-loader';
import {
  AppConfig,
  LeaseCostEstimatorPage,
  LeaseCostEstimatorQuote,
  LeaseEstimatesState,
  Onboarding,
  OnboardingState,
  StoreAddress,
  StoreLocationData
} from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { chance } from 'jest-chance';
import { Subject, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { LeaseCostEstimatorInjectable } from './lease-cost-estimator-injectable';
import { LeaseCostEstimatorManagerComponent } from './lease-cost-estimator-manager.component';

describe('LeaseCostEstimatorManagerComponent', () => {
  let component: LeaseCostEstimatorManagerComponent;

  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;
  let mockRenderer: SubstituteOf<Renderer2>;
  let mockLeaseEstimatesState: SubstituteOf<LeaseEstimatesState>;
  let mockLeaseCostEstimator: SubstituteOf<LeaseCostEstimatorInjectable>;
  let mockDocument: SubstituteOf<Document>;
  let mockConfig: SubstituteOf<AppConfig>;
  let mockLayoutService: SubstituteOf<LayoutService>;

  beforeEach(() => {
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockOnboardingState = Substitute.for<OnboardingState>();
    mockLeaseEstimatesState = Substitute.for<LeaseEstimatesState>();
    mockRenderer = Substitute.for<Renderer2>();
    mockLeaseCostEstimator = Substitute.for<LeaseCostEstimatorInjectable>();
    mockConfig = Substitute.for<AppConfig>();
    mockDocument = Substitute.for<Document>();
    mockLayoutService = Substitute.for<LayoutService>();

    component = new LeaseCostEstimatorManagerComponent(
      mockApplyRouter,
      mockOnboardingState,
      mockLeaseEstimatesState,
      mockRenderer,
      mockLeaseCostEstimator,
      mockConfig,
      mockLayoutService,
      mockDocument,
    );
    component.leaseCostEstimatorPortal = Substitute.for<ElementRef>();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('On Init', () => {
    beforeEach(() => {
      component.ngOnInit();
    });

    it('should add class to body to accommodate LCE design', () => {
      mockRenderer
        .received()
        .addClass(
          mockDocument.body,
          LeaseCostEstimatorManagerComponent.bodyClass
        );
    });

    it('should show back button', () => {
      mockLayoutService.received().showHeaderBackButton();
    });

    it('should show exit button', () => {
      mockLayoutService.received().showHeaderExitButton();
    });

    it('should hide footer', () => {
      mockLayoutService.received().hideFooter();
    });
  });

  describe('Payment Estimator Tool Management', () => {
    let quoteSubject: Subject<LeaseCostEstimatorQuote>;
    let lcePageSubject: Subject<LeaseCostEstimatorPage>;
    let onboardingSubject: Subject<Onboarding>;

    beforeEach(() => {
      quoteSubject = new Subject<LeaseCostEstimatorQuote>();
      lcePageSubject = new Subject<LeaseCostEstimatorPage>();
      onboardingSubject = new Subject<Onboarding>();
      mockLeaseEstimatesState.leaseCostEstimatorQuote$.returns(
        quoteSubject.asObservable()
      );
      mockLeaseEstimatesState.leaseCostEstimatorPage$.returns(
        lcePageSubject.asObservable()
      );
      mockOnboardingState.onboardingInfo$.returns(onboardingSubject.asObservable());
    });

    describe('On AfterViewInit', () => {
      let mockToolTemplate: SubstituteOf<AppLoadResult>;
      let onboardingInfo: Onboarding;

      beforeEach(() => {
        mockToolTemplate = Substitute.for<AppLoadResult>();

        mockLeaseCostEstimator
          .loadMicroFrontend(Arg.any())
          .returns(Promise.resolve(mockToolTemplate));
      });

      describe('Given the tool is already displayed', () => {
        describe('Given onboarding info is available', () => {
          onboardingInfo = {
            storeId: chance.integer(),
            languageCode: chance.string()
          } as Onboarding;

          it('should not reload the UI', () => {
            component.ngAfterViewInit();
            lcePageSubject.next(LeaseCostEstimatorPage.tool);
            quoteSubject.next(undefined);
            onboardingSubject.next(onboardingInfo);

            mockLeaseCostEstimator.didNotReceive().loadMicroFrontend(Arg.any());
          });
        });

        describe('Given onboarding info is unavailable', () => {
          onboardingInfo = {} as Onboarding;

          it('should not reload the UI', () => {
            component.ngAfterViewInit();
            lcePageSubject.next(LeaseCostEstimatorPage.tool);
            quoteSubject.next(undefined);
            onboardingSubject.next(onboardingInfo);

            mockLeaseCostEstimator.didNotReceive().loadMicroFrontend(Arg.any());
          });
        });
      });

      describe('Given the tool is not yet displayed', () => {
        describe('Given onboarding info is available', () => {
          onboardingInfo = {
            storeId: chance.integer(),
            languageCode: chance.string(),
            storeLocation: {
              address: {
                state: chance.state()
              }
            } as StoreLocationData
          } as Onboarding;

          it('should load the UI', () => {
            component.ngAfterViewInit();
            lcePageSubject.next(undefined);
            quoteSubject.next(undefined);
            onboardingSubject.next(onboardingInfo);

            mockLeaseCostEstimator.received().loadMicroFrontend(Arg.any());
          });
        });

        describe('Given partial onboarding info', () => {
          it('should not load the UI without a store id', () => {
            onboardingInfo = {
              storeId: undefined,
              languageCode: chance.string(),
              storeLocation: {
                address: {
                  state: chance.state()
                }
              } as StoreLocationData
            } as Onboarding;

            component.ngAfterViewInit();
            lcePageSubject.next(undefined);
            quoteSubject.next(undefined);
            onboardingSubject.next(onboardingInfo);

            mockLeaseCostEstimator.didNotReceive().loadMicroFrontend(Arg.any());
          });

          it('should not load the UI without a language code', () => {
            onboardingInfo = {
              storeId: chance.integer(),
              languageCode: undefined,
              storeLocation: {
                address: {
                  state: chance.state()
                }
              } as StoreLocationData
            } as Onboarding;

            component.ngAfterViewInit();
            lcePageSubject.next(undefined);
            quoteSubject.next(undefined);
            onboardingSubject.next(onboardingInfo);

            mockLeaseCostEstimator.didNotReceive().loadMicroFrontend(Arg.any());
          });

          it('should not load the UI without a state', () => {
            onboardingInfo = {
              storeId: chance.integer(),
              languageCode: undefined,
              storeLocation: {
                address: {
                  state: undefined
                }
              } as StoreLocationData
            } as Onboarding;

            component.ngAfterViewInit();
            lcePageSubject.next(undefined);
            quoteSubject.next(undefined);
            onboardingSubject.next(onboardingInfo);

            mockLeaseCostEstimator.didNotReceive().loadMicroFrontend(Arg.any());
          });

        });
      });
    });

    describe('On Destroy', () => {
      let mockSubscription: SubstituteOf<Subscription>;

      beforeEach(() => {
        mockSubscription = Substitute.for<Subscription>();
        component.subscriptions = mockSubscription;
      });

      it('should remove class from body that accommodated LCE design', () => {
        component.ngOnDestroy();

        mockRenderer
          .received()
          .removeClass(
            mockDocument.body,
            LeaseCostEstimatorManagerComponent.bodyClass
          );
      });

      it('should unsubscribe from any subscriptions', () => {
        component.ngOnDestroy();

        mockSubscription.received().unsubscribe();
      });
    });
  });
});