import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import {
  ApplyPageKey,
  AuthState,
  OnboardingState
} from '@ua/shared/data-access';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApplyRouter } from './apply-router';
@Injectable()
export class OnboardingGuard implements CanActivate {
  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly onboardingState: OnboardingState,
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.onboardingState.onboardingGuid$.pipe(
      map(guid => {
        const onboardingParam = route.queryParamMap.get('onboardingGuid');
        const storedOnboardingGuid = sessionStorage.getItem('onboardingGuid');

        if (onboardingParam && onboardingParam !== storedOnboardingGuid) {
          this.onboardingState.requestOnboardingInfo(onboardingParam);
          sessionStorage.setItem('onboardingGuid', onboardingParam);
          return true;
        }

        if (storedOnboardingGuid) {
          this.onboardingState.requestOnboardingInfo(storedOnboardingGuid);
          return true;
        }

        this.applyRouter.navigate(ApplyPageKey.notFound);
        return false;
      })
    );
  }
}