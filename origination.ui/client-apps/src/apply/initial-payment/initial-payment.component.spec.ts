import { ChangeDetectorRef } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Arg, Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import {
  Address, Application,
  ApplicationState,
  ApplyPageKey,
  CreditCardInformation,
  Customer,

  CustomerState,
  LocationService
} from '@ua/shared/data-access';
import {
  cityValidator,
  creditCard,
  expirationDate,
  nameFormat,
  startsWithLetter,
  stateValidator,
  street1Validator,
  zipValidator
} from '@ua/shared/utilities';
import { chance } from 'jest-chance';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import {
  InitialPaymentComponent,
  InitialPaymentFormFields
} from './initial-payment.component';
import { LayoutService } from '@ua/shared/ui';

describe('InitialPaymentComponent', () => {
  let component: InitialPaymentComponent;

  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockApplicationState: SubstituteOf<ApplicationState>;
  let mockCustomerState: SubstituteOf<CustomerState>;
  let mockFormBuilder: SubstituteOf<FormBuilder>;
  let mockLocation: SubstituteOf<LocationService>;
  let mockCD: SubstituteOf<ChangeDetectorRef>;
  let mockLayoutService: SubstituteOf<LayoutService>;

  beforeEach(() => {
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockApplicationState = Substitute.for<ApplicationState>();
    mockCustomerState = Substitute.for<CustomerState>();
    mockFormBuilder = Substitute.for<FormBuilder>();
    mockLocation = Substitute.for<LocationService>();
    mockCD = Substitute.for<ChangeDetectorRef>();
    mockLayoutService = Substitute.for<LayoutService>();

    component = new InitialPaymentComponent(
      mockApplyRouter,
      mockApplicationState,
      mockCustomerState,
      mockFormBuilder,
      mockLocation,
      mockCD,
      mockLayoutService
    );
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('Update Customer', () => {
    let customerSubject: BehaviorSubject<Customer>;
    let cardSubject: BehaviorSubject<CreditCardInformation>;
    let mockFormGroup: SubstituteOf<FormGroup>;
    let mockFirstNameController: SubstituteOf<FormControl>;
    let mockLastNameController: SubstituteOf<FormControl>;
    const mockFirstName = chance.string();
    const mockLastName = chance.string();

    beforeEach(() => {
      mockFirstNameController = Substitute.for<FormControl>();
      mockLastNameController = Substitute.for<FormControl>();
      mockFormGroup = Substitute.for<FormGroup>();
      mockFormGroup
        .get(InitialPaymentFormFields.firstNameOnCard)
        .returns(mockFirstNameController);
      mockFormGroup
        .get(InitialPaymentFormFields.lastNameOnCard)
        .returns(mockLastNameController);
      mockFormBuilder.group(Arg.any(), Arg.any()).returns(mockFormGroup);
      cardSubject = new BehaviorSubject<CreditCardInformation>({
        firstName: '',
        lastName: '',
      } as CreditCardInformation);
      mockCustomerState.card$.returns(cardSubject.asObservable());
      customerSubject = new BehaviorSubject<Customer>({ customerId: 0 } as Customer);
      mockCustomerState.customer$.returns(customerSubject.asObservable());
    });

    it('should update name fields from customer if cc not available', () => {
      component.ngOnInit();

      customerSubject.next({
        firstName: mockFirstName,
        lastName: mockLastName,
      } as Customer);
      mockFirstNameController.received().setValue(mockFirstName);
      mockLastNameController.received().setValue(mockLastName);
    });

    describe('given neither the cc nor the customer have a name ', () => {
      it('it should not update name fields from customer', () => {
        component.ngOnInit();

        customerSubject.next({
          firstName: '',
          lastName: '',
        } as Customer);
        cardSubject.next({
          firstName: '',
          lastName: '',
        } as CreditCardInformation);
        mockFirstNameController.didNotReceive().setValue(Arg.any());
        mockLastNameController.didNotReceive().setValue(Arg.any());
      });
    });

    it('should update name fields from cc if available', () => {
      component.ngOnInit();

      customerSubject.next({
        firstName: chance.string(),
        lastName: chance.string(),
      } as Customer);
      cardSubject.next({
        firstName: mockFirstName,
        lastName: mockLastName,
      } as CreditCardInformation);
      mockFirstNameController.received().setValue(mockFirstName);
      mockLastNameController.received().setValue(mockLastName);
    });
  });

  describe('On Init', () => {
    let applicationSubject: BehaviorSubject<Application>;
    let cardSubject: BehaviorSubject<CreditCardInformation>;
    let mockFormGroup: SubstituteOf<FormGroup>;

    beforeEach(() => {
      applicationSubject = new BehaviorSubject<Application>({} as Application);
      cardSubject = new BehaviorSubject<CreditCardInformation>(
        {} as CreditCardInformation
      );
      mockApplicationState.application$.returns(
        applicationSubject.asObservable()
      );
      mockCustomerState.card$.returns(cardSubject.asObservable());

      mockFormGroup = Substitute.for<FormGroup>();
      mockFormBuilder.group(Arg.any(), Arg.any()).returns(mockFormGroup);
    });

    it('should keep track of subscriptions', () => {
      expect(component.subs.length).toBe(0);
      component.ngOnInit();

      expect(component.subs.length).toBeGreaterThan(0);
    });

    it('should add first name field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.firstName[1];
          return (
            validators.includes(Validators.required) &&
            validators.includes(nameFormat) &&
            validators.includes(startsWithLetter)
          );
        }),
        Arg.any()
      );
    });

    it('should add last name field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.lastName[1];
          return (
            validators.includes(Validators.required) &&
            validators.includes(nameFormat) &&
            validators.includes(startsWithLetter)
          );
        }),
        Arg.any()
      );
    });

    it('should add credit card field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.cardNumber[1];
          return (

            validators.includes(creditCard)
          );
        }),
        Arg.any()
      );
    });

    it('should add exp date field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.expDate[1];
          return (
            validators.includes(Validators.required) &&
            validators.includes(expirationDate)
          );
        }),
        Arg.any()
      );
    });

    it('should add street1 field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.street1[1];
          return validators.includes(street1Validator);
        }),
        Arg.any()
      );
    });

    it('should add street2 field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.street2[1];
          return validators.includes(street1Validator);
        }),
        Arg.any()
      );
    });

    it('should add city field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.city[1];
          return validators.includes(cityValidator);
        }),
        Arg.any()
      );
    });

    it('should add state field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.state[1];
          return validators.includes(stateValidator);
        }),
        Arg.any()
      );
    });

    it('should add zip field validity', () => {
      component.ngOnInit();
      mockFormBuilder.received().group(
        Arg.is(x => {
          const validators = x.zip[1];
          return validators.includes(zipValidator);
        }),
        Arg.any()
      );
    });
  });

  describe('Subscription cleanup', () => {
    let mockSubscription: SubstituteOf<Subscription>;

    beforeEach(() => {
      const observable = Substitute.for<Observable<any>>();
      mockSubscription = Substitute.for<Subscription>();
      observable.subscribe().returns(mockSubscription);
      component.subs.push(observable.subscribe());
    });

    it('should unsubscribe the open subs', () => {
      mockSubscription.closed.returns(false);
      component.ngOnDestroy();

      mockSubscription.received().unsubscribe();
    });

    it('should not unsubscribe the closed subs', () => {
      mockSubscription.closed.returns(true);
      component.ngOnDestroy();

      mockSubscription.didNotReceive().unsubscribe();
    });
  });

  describe('Mask changes', () => {
    it('should toggle the masking', () => {
      const masking = chance.bool();
      component.onMaskChanged(masking);

      expect(component.isMasked).toBe(masking);
    });
  });

  describe('Disallowing NonDigital Input', () => {
    let keyPressEvent;

    beforeEach(() => {
      keyPressEvent = {};
    });

    it('should reject alpha char codes', () => {
      keyPressEvent.charCode = chance.string();
      const result = component.disallowNonDigitInput(keyPressEvent);
      expect(result).toBe(false);
    });

    it('should reject alpha key codes', () => {
      keyPressEvent.charCode = undefined;
      keyPressEvent.keyCode = chance.string();
      const result = component.disallowNonDigitInput(keyPressEvent);
      expect(result).toBe(false);
    });

    it('should allow numerical char codes', () => {
      keyPressEvent.charCode = chance.pickone([
        48,
        49,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
        57,
      ]);
      const result = component.disallowNonDigitInput(keyPressEvent);
      expect(result).toBeUndefined();
    });

    it('should allow numerical key codes', () => {
      keyPressEvent.charCode = undefined;
      keyPressEvent.keyCode = chance.pickone([
        48,
        49,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
        57,
      ]);
      const result = component.disallowNonDigitInput(keyPressEvent);
      expect(result).toBeUndefined();
    });
  });

  describe('After View Init', () => {
    let mockFormGroup: SubstituteOf<FormGroup>;

    beforeEach(() => {
      mockFormGroup = Substitute.for<FormGroup>();

      component.initialPaymentForm = mockFormGroup;
    });

    it('should enable timout feature', () => {
      component.ngAfterViewInit();
      mockLayoutService.received().enableTimeout();
    });
  });

  describe('Continue', () => {
    beforeEach(() => {
      component.initialPaymentForm = new FormGroup({
        [InitialPaymentFormFields.firstNameOnCard]: new FormControl(),
        [InitialPaymentFormFields.lastNameOnCard]: new FormControl(),
        [InitialPaymentFormFields.cardNumber]: new FormControl(),
        [InitialPaymentFormFields.cardExpiration]: new FormControl(),
      });
    });

    it('should update the application', () => {
      const firstName = chance.string();
      const lastName = chance.string();
      const cardNumber = chance.integer();
      const cardExpiration = chance.string();

      component.initialPaymentForm
        .get(InitialPaymentFormFields.firstNameOnCard)
        .setValue(firstName);
      component.initialPaymentForm
        .get(InitialPaymentFormFields.lastNameOnCard)
        .setValue(lastName);
      component.initialPaymentForm
        .get(InitialPaymentFormFields.cardNumber)
        .setValue(cardNumber);
      component.initialPaymentForm
        .get(InitialPaymentFormFields.cardExpiration)
        .setValue(cardExpiration);

      mockCustomerState.submitCreditCard().resolves(true);

      component.customer = {
        customerId: 0, address: { address1: 'test', address2: 'test', city: 'test', state: 'UT', zip: '84045' } as Address
      } as Customer;
      component.onContinue();
      mockCustomerState.received().updateCreditCard({
        customerId: 0,
        firstName,
        lastName,
        cardNumber,
        expirationDate: cardExpiration,
        streetAddress1: component.customer.address.address1,
        streetAddress2: component.customer.address.address2,
        city: component.customer.address.city,
        state: component.customer.address.state,
        zip: component.customer.address.zip,
        bin: '',
        token: '',
      } as CreditCardInformation);
    });

    it('should go to the next page if not in edit mode', () => {
      mockLayoutService.editMode.returns(false);
      component.handleCardSubmitted(true);

      mockApplyRouter.received().stepForward();
    });

    it('should go to the review page if in edit mode', () => {
      mockLayoutService.editMode.returns(true);
      component.handleCardSubmitted(true);

      mockApplyRouter.received().navigate(ApplyPageKey.review);
    });

  });
});