import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { OnboardingState } from '@ua/shared/data-access';
import { chance } from 'jest-chance';
import { Subject } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { ApplyLayoutComponent } from './apply-layout.component';
import { Idle } from '@ng-idle/core';
import { AnalyticsService } from '@ua/shared/analytics';
import { LayoutService } from '@ua/shared/ui';
import { TranslateService } from '@ngx-translate/core';

describe('ApplyLayoutComponent', () => {
  let component: ApplyLayoutComponent;
  let mockApplyRouter: SubstituteOf<ApplyRouter>;
  let mockOnboardingState: SubstituteOf<OnboardingState>;
  let mockIdleService: SubstituteOf<Idle>;
  let mockAnalyticsService: SubstituteOf<AnalyticsService>;
  let mockLayoutService: SubstituteOf<LayoutService>;
  let mockTranslateService: SubstituteOf<TranslateService>;

  beforeEach(() => {
    mockApplyRouter = Substitute.for<ApplyRouter>();
    mockOnboardingState = Substitute.for<OnboardingState>();
    mockIdleService = Substitute.for<Idle>();
    mockAnalyticsService = Substitute.for<AnalyticsService>();
    mockLayoutService = Substitute.for<LayoutService>();
    mockTranslateService = Substitute.for<TranslateService>();

    component = new ApplyLayoutComponent(
      mockApplyRouter,
      mockOnboardingState,
      mockIdleService,
      mockAnalyticsService,
      mockLayoutService,
      mockTranslateService
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('On Init', () => {
    let languageCodeSubject: Subject<string>;

    beforeEach(() => {
      languageCodeSubject = new Subject<string>();
      mockOnboardingState.languageCode$.returns(languageCodeSubject.asObservable());
      component.ngOnInit();
    });

    it('should get current localization from onboarding state', () => {
      const expected = chance.pickone(['en', 'es', 'fr']);
      let actual: string;
      component.currentLocalization$.subscribe(lang => {
        actual = lang;
      });
      languageCodeSubject.next(expected);
      expect(actual).toBe(expected);
    });
  });

  describe('On Back', () => {
    it('should dispatch the event created from walk back', () => {
      component.onBack();

      mockApplyRouter.received().stepBack();
    });
  });

  describe('On Close', () => {
    it('should dispatch the event created from navigate route', () => {
      component.onClose();

      mockApplyRouter.received().restartApplication();
    });
  });

  describe('On Logo Click', () => {
    it('should dispatch the event created from navigate route', () => {
      component.onLogoClick();

      mockApplyRouter.received().restartApplication();
    });
  });

  describe('On Change Language', () => {
    it('should set the preferred language in the onboarding state', () => {
      const languageCode = chance.pickone(['en', 'es', 'fr']);
      component.onChangeLanguage(languageCode);

      mockOnboardingState.received().setLanguage(languageCode);
    });
  });

});