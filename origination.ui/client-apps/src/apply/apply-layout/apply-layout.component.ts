import { Component, OnInit } from '@angular/core';
import { DocumentInterruptSource, Idle } from '@ng-idle/core';
import { AnalyticsService } from '@ua/shared/analytics';
import { OnboardingState } from '@ua/shared/data-access';
import { LayoutService } from '@ua/shared/ui';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApplyRouter } from '../apply-router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ua-apply-layout',
  templateUrl: './apply-layout.component.html',
})
export class ApplyLayoutComponent implements OnInit {
  currentLocalization$: Observable<string>;
  private timeTilIdle = 840;
  private timeTilTimeout = 60;
  private idleEnabled = false;

  private subs: Subscription[] = [];

  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly onboardingState: OnboardingState,
    private readonly idleService: Idle,
    private readonly analyticsService: AnalyticsService,
    private readonly layoutService: LayoutService,
    private readonly translateService: TranslateService
  ) { }

  ngOnInit() {
    this.initializeIdle();

    this.currentLocalization$ = this.onboardingState.languageCode$;
    this.currentLocalization$.subscribe(langCode => this.translateService.use(langCode));
  }

  onBack() {
    this.applyRouter.stepBack();
  }

  onClose() {
    this.applyRouter.restartApplication();
  }

  onLogoClick() {
    this.applyRouter.restartApplication();
  }

  onChangeLanguage(desiredLanguage) {
    this.onboardingState.setLanguage(desiredLanguage);
  }

  public onInterruptIdle(requestLogout: boolean): void {
    this.resetIdle();

    if (!requestLogout) { return; };

    this.applyRouter.restartApplication();
    this.analyticsService.sessionTimeoutEvent();
  }

  private startIdle(): void {
    if (!this.idleEnabled) { return; }
    this.idleService.clearInterrupts();
    this.layoutService.showTimeoutPromptButton();
  }

  private resetIdle(): void {
    this.layoutService.hideTimeoutPromptButton();
    // eslint-disable-next-line max-len
    this.idleService.setInterrupts([new DocumentInterruptSource('keydown DOMMouseScroll mousewheel mousedown touchstart touchmove scroll')]);
    this.idleService.watch();
  }

  private initializeIdle(): void {
    this.layoutService.layoutElements$.subscribe(x => this.idleEnabled = x.timeoutEnabled);
    this.idleService.setTimeout(this.timeTilTimeout);
    this.idleService.setIdle(this.timeTilIdle);
    // eslint-disable-next-line max-len
    this.subs.push(this.idleService.onIdleStart.subscribe(() => this.startIdle()),
      this.idleService.onIdleEnd.subscribe(() => this.resetIdle()),
      this.idleService.onTimeout.subscribe(() => this.onInterruptIdle(true))
    );
    this.resetIdle();
  }
}