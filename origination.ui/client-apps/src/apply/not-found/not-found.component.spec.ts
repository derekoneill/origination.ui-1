import Substitute, { SubstituteOf } from '@fluffy-spoon/substitute';
import { LayoutService } from '@ua/shared/ui';
import { NotFoundComponent } from '../not-found/not-found.component';

describe('NotFoundComponent', () => {
    let component: NotFoundComponent;

    let mockLayoutService: SubstituteOf<LayoutService>;

    beforeEach(() => {
        mockLayoutService = Substitute.for<LayoutService>();

        component = new NotFoundComponent(mockLayoutService);
    });

    it('should create', () => {
        expect(component).toBeDefined();
    });

    describe('on init', () => {
        beforeEach(() => {
            component.ngOnInit();
        });

        it('should hide the footer', () => {
            mockLayoutService.received().hideFooter();
        });

        it('should hide the exit button', () => {
            mockLayoutService.received().hideHeaderExitButton();
        });

        it('should hide the back button', () => {
            mockLayoutService.received().hideHeaderBackButton();
        });
    });
});