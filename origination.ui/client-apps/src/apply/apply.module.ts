import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { CommonModule, CurrencyPipe, DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GritUniversalModule } from '@progleasing/grit-universal-angular';
import { AnalyticsModule } from '@ua/shared/analytics';
import { ComponentsModule } from '@ua/shared/ui';
import { TextMaskModule } from 'angular2-text-mask';
import { InputTrimModule } from 'ng2-trim-directive';
import { ApplyLayoutComponent } from './apply-layout/apply-layout.component';
import { ApplyPageTemplateComponent } from './apply-page-template/apply-page-template.component';
import { ApplyRouter } from './apply-router';
import { ApplyRoutingModule } from './apply.routing.module';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LeaseCostEstimatorInjectable } from './lease-cost-estimator-manager/lease-cost-estimator-injectable';
import { LeaseCostEstimatorManagerComponent } from './lease-cost-estimator-manager/lease-cost-estimator-manager.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OnboardingGuard } from './onboarding-guard';
import { PreSubmitAppTermsSheetManagerComponent } from './pre-submit-app-terms-sheet-manager/pre-submit-app-terms-sheet-manager.component';
import { ResultsComponent } from './results/results.component';
import { ReviewComponent } from './review/review.component';
import { HttpErrorInterceptor } from './HttpErrorInterceptor';
import { TranslateModule } from '@ngx-translate/core';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { InlineTranslatePipe } from './inline-translate.pipe';
import { PrefillComponent } from './prefill/prefill.component';

@NgModule({
  imports: [
    AnalyticsModule,
    CommonModule,
    ComponentsModule,
    ApplyRoutingModule,
    TextMaskModule,
    InputTrimModule,
    ReactiveFormsModule,
    GritUniversalModule,
    TranslateModule,
  ],
  providers: [
    ApplyRouter,
    CurrencyPipe,
    DatePipe,
    LeaseCostEstimatorInjectable,
    OnboardingGuard,
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true, deps: [HttpClient, ApplyRouter] },
  ],
  declarations: [
    ApplyRoutingModule.components,
    ApplyLayoutComponent,
    ApplyPageTemplateComponent,
    ResultsComponent,
    PreSubmitAppTermsSheetManagerComponent,
    LandingPageComponent,
    ReviewComponent,
    LeaseCostEstimatorManagerComponent,
    NotFoundComponent,
    TermsAndConditionsComponent,
    PrefillComponent,
    InlineTranslatePipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ApplyModule { }