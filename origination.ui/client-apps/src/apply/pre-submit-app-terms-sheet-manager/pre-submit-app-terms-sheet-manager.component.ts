import { AfterViewInit, Component, OnInit } from '@angular/core';
import {
  Application,
  ApplicationState,
  ApplyPageKey
} from '@ua/shared/data-access';
import { Subscription } from 'rxjs';
import { ApplyRouter } from '../apply-router';
import { LayoutService } from '@ua/shared/ui';

@Component({
  selector: 'ua-pre-submit-app-terms-sheet-manager',
  templateUrl: './pre-submit-app-terms-sheet-manager.component.html',
  styleUrls: ['./pre-submit-app-terms-sheet-manager.scss'],

})
export class PreSubmitAppTermsSheetManagerComponent implements OnInit, AfterViewInit {
  subs: Subscription[] = [];
  application: Application;
  pageRoute = ApplyPageKey.submitApplicationTerms;
  constructor(
    private readonly applyRouter: ApplyRouter,
    private readonly applicationState: ApplicationState,
    private readonly layoutService: LayoutService
  ) { }

  ngOnInit(): void {
    this.subs.push(
      this.applicationState.application$.subscribe(
        application => (this.application = application)
      )
    );
  }

  ngAfterViewInit(): void {
    this.layoutService.enableTimeout();
  }

  onContinue() {
    this.applyRouter.stepForward();
  }
}