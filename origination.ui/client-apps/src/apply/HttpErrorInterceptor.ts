import { HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { ApplyPageKey } from '@ua/shared/data-access';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ApplyRouter } from './apply-router';

export class LogRequest {
  public logLevel: number;
  public message: string;
  public details: Record<string, unknown>;
}

export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private http: HttpClient, private router: ApplyRouter) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            errorMessage = `Error: ${error.error.message}`;
          }
          else {
            errorMessage = `Error code: ${error.status}\nMessage: ${error.message}`;
          }
          const logError = {
            logLevel: 3,
            message: 'Origination UI Client connection error',
            details: {
              path: request.url,
              queryParams: request.params.toString(),
              responseCode: error.status.toString(),
              error: errorMessage
            }

          } as LogRequest;
          if (request.url.includes('http')) {
            this.logError(logError);
          }
          this.router.navigate(ApplyPageKey.notFound);
          return throwError(errorMessage);
        })
      );
  }

  logError(request: LogRequest): void {
    const uri = `api/log`;
    this.http.post<LogRequest>(uri, request).subscribe();
  }
}