using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace origination.ui.ConfigService
{
    [Route("api/configs")]
    [ApiController]
    public class ConfigController : ControllerBase
    {
        private readonly IConfiguration configuration;
        public ConfigController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpGet("services")]
        public Dictionary<string, string> GetServicesConfiguration()
        {
            return ServiceConfiguration.GetProgApplyUrls(configuration.GetProgEnvironment())
                .Select(x => new KeyValuePair<string, string>(Enum.GetName(typeof(ServiceName), x.Key), x.Value))
                .ToDictionary(n => n.Key.Substring(0, 1).ToLower() + n.Key.Substring(1), n => n.Value);
        }
    }
}