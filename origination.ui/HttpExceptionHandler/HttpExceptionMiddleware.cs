using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ProgLeasing.System.Logging.Contract;

namespace origination.ui.HttpExceptionHandler
{
    public class HttpExceptionMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger<HttpExceptionMiddleware> logger;

        public HttpExceptionMiddleware(
            RequestDelegate next,
            ILogger<HttpExceptionMiddleware> logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await next(httpContext);
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, "Exception occurred", exception);
                await HandleExceptionAsync(httpContext, exception);
            }
        }

        private async Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            httpContext.Response.ContentType = "application/json";
            if (exception is HttpException httpException)
            {
                httpContext.Response.StatusCode = httpException.StatusCode;
            }
            else
            {
                httpContext.Response.StatusCode = 500;
            }

            await httpContext.Response.WriteAsync(new HttpErrorDetail
            {
                Message = exception.Message
            }.ToString());
        }
    }
}