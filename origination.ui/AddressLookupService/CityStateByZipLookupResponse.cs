using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace origination.ui.AddressLookupService
{
    public class CityStateByZipLookupResponse
    {
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
