
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace origination.ui.AddressLookupService
{
    public class AddressLookupProvider : IAddressLookupProvider
    {
        private readonly HttpClient _httpClient;

        public AddressLookupProvider(HttpClient client)
        {
            _httpClient = client;
        }

        public async Task<CityStateByZipLookupResponse> GetCityAndStateByZip(string zipcode)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new System.Uri($"{_httpClient.BaseAddress.AbsoluteUri}&components=country:US|postal_code:{zipcode}")
            };
            var response = await _httpClient.SendAsync(request);

            var result =
                await JsonSerializer.DeserializeAsync<GoogleGeocodeApiResponse>(await response.Content.ReadAsStreamAsync());

            if (result.status != null &&  result.status.ToLower().Equals("ok"))
            {
                CityStateByZipLookupResponse cityState = new CityStateByZipLookupResponse();
                cityState.Zip = zipcode;

                foreach (AddressComponent geocodeResult in result.results[0].address_components)
                {
                    if (geocodeResult.types.Any(t => t.Equals("locality", System.StringComparison.OrdinalIgnoreCase)))
                    {
                        cityState.City = geocodeResult.short_name;
                    }
                    if (geocodeResult.types.Any(t => t.Equals("administrative_area_level_1", System.StringComparison.OrdinalIgnoreCase)))
                    {
                        cityState.State = geocodeResult.short_name;
                    }
                }
                return cityState;
            }

            return null;
        }
    }
}
