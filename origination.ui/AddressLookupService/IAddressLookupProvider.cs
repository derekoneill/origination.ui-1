using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace origination.ui.AddressLookupService
{
    public interface IAddressLookupProvider
    {
        Task<CityStateByZipLookupResponse> GetCityAndStateByZip(string zipcode);
    }
}
