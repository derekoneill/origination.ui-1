using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using origination.ui;

namespace origination.ui.AddressLookupService
{
    [Route("api/addressLookup")]
    public class AddressLookupController : ControllerBase
    {
        private readonly IAddressLookupProvider _provider;
        private readonly IConfiguration configuration;

        public AddressLookupController(IConfiguration configuration, IAddressLookupProvider addressLookupProvider)
        {
            _provider = addressLookupProvider;
            this.configuration = configuration;
        }

        /// <summary>
        /// Searches for a City and State that match a supplied zip code.
        /// State is always returned as the 2 character abbreviation.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /addressLookup?zip=12345
        ///
        /// </remarks>
        /// <param name="zip"></param>
        /// <returns>City, state and zipcode</returns>
        /// <response code="201">No matches found for given zip</response>
        /// <response code="200">Found a matching City and State</response>  
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<CityStateByZipLookupResponse> GetCityAndStateByZip(string zip)
        {
            return await _provider.GetCityAndStateByZip(zip);
        }

        /// <summary>
        /// just a test
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET 
        ///
        /// </remarks>
        /// <param name="zip"></param>
        /// <returns>environment</returns>
        /// <response code="200">it didn't break</response>
        [HttpGet]
        [Route("test")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ProgEnvironment Test(string zip)
        {
            return this.configuration.GetProgEnvironment();
        }
    }
}
