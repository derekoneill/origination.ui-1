using System.Collections.Generic;

namespace origination.ui.AddressLookupService
{
    public class GoogleGeocodeApiResponse
    {
        public List<GeocodeResult> results { get; set; }
        public string status { get; set; }
    }

    public class GeocodeResult
    {
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address{ get; set; }

        public string place_id { get; set; }
    }

    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }

    }

    public class Geometry
    {
        public Boundary bounds { get; set; }
        public GeoCoords location { get; set; }
        public string location_type { get; set; }
        public Boundary viewport { get; set; }
    }

    public class Boundary
    {
        public GeoCoords northeast { get; set; }
        public GeoCoords southwest { get; set; }
    }

    public class GeoCoords
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
