using System;
using Microsoft.Extensions.DependencyInjection;
using origination.ui.ConfigService;

namespace origination.ui.AddressLookupService
{
    public static class AddressLookupStartup
    {
        public static void AddAddressLookupProvider(this IServiceCollection services, ServiceConfiguration serviceConfiguration)
        {
            services.AddHttpClient<IAddressLookupProvider, AddressLookupProvider>(client =>
            {
                client.BaseAddress = new Uri(serviceConfiguration.AddressLookupUrl);
            });
            
        }
    }
}
