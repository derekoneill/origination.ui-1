using Microsoft.AspNetCore.Mvc;
using origination.ui.LogService;
using ProgLeasing.System.Logging.Contract;
using System;
using System.Threading.Tasks;

namespace origination.ui.ConfigService
{
    [Route("api/log")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private readonly ILogger<LogController> logger;
        public LogController(ILogger<LogController> newLogger)
        {
            logger = newLogger;
        }

        [HttpPost]
        public IActionResult Log(LogRequest req)
        {
            logger.Log(req.LogLevel, req.Message,null, req.Details);
            return Ok();
        }
    }
}